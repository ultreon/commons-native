import org.jetbrains.kotlin.backend.common.serialization.cityHash64

buildscript {
    repositories {
        mavenLocal()
        google()
        maven { url = uri("https://dl.bintray.com/korlibs/korlibs") }
        maven { url = uri("https://plugins.gradle.org/m2/") }
        mavenCentral()
        google()
    }
    dependencies {

    }
}

plugins {
    kotlin("multiplatform") version "1.7.0"
    kotlin("plugin.serialization") version "1.7.0"
}

group = "tk.ultreonteam.commons-native"
version = "1.0.0"

repositories {
    mavenCentral()
    mavenLocal()
    google()
    jcenter()

    maven(url = "https://dl.bintray.com/animeshz/maven")
}

kotlin {
//    val hostOs = System.getProperty("os.name")
//    val isMingwX64 = hostOs.startsWith("Windows")
//    val nativeTarget = when {
//        hostOs == "Mac OS X" -> macosX64("native")
//        hostOs == "Linux" -> linuxX64("native")
//        isMingwX64 -> mingwX64("native")
//        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
//    }
//
//    nativeTarget.apply {
//        binaries {
//            executable {
//                entryPoint = "main"
//            }
//        }
//    }

    mingwX64().apply {
        binaries {
            sharedLib {
                baseName = "libultreon-commons"
            }
        }
    }
    linuxX64().apply {
        binaries {
            sharedLib {
                baseName = "ultreon-commons"
            }
        }
    }
    macosX64().apply {
        binaries {
            sharedLib {
                baseName = "ultreon-commons"
            }
        }
    }

    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.3.2")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${properties["kotlinx.serialization.json.version"]}")
                implementation("com.soywiz.korlibs.korio:korio:${properties["korlibs.version"]}")
                implementation("com.soywiz.korlibs.korgw:korgw:${properties["korlibs.version"]}")
                implementation("io.ktor:ktor-client-json:${properties["ktor-client-json.version"]}")
//                implementation("com.github.animeshz:mouse-kt-mingwX64:0.3.3")
            }
        }
        val commonTest by getting {
            dependsOn(commonMain)
            dependencies {
                implementation(kotlin("test-common"))
            }
        }
        val mingwX64Main by sourceSets.getting {
            dependsOn(commonMain)
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json-mingwx64:${properties["kotlinx.serialization.json.version"]}")
                implementation("com.soywiz.korlibs.korio:korio-mingwx64:${properties["korlibs.version"]}")
                implementation("com.soywiz.korlibs.korgw:korgw-mingwx64:${properties["korlibs.version"]}")
                implementation("io.ktor:ktor-client-json-mingwx64:${properties["ktor-client-json.version"]}")
            }
        }

        val mingwX64Test by sourceSets.getting {
            dependsOn(commonTest)
            dependsOn(mingwX64Main)
        }

        val linuxX64Main by sourceSets.getting {
            dependsOn(commonMain)
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json-linuxx64:${properties["kotlinx.serialization.json.version"]}")
                implementation("com.soywiz.korlibs.korio:korio-linuxx64:${properties["korlibs.version"]}")
                implementation("com.soywiz.korlibs.korgw:korgw-linuxx64:${properties["korlibs.version"]}")
                implementation("io.ktor:ktor-client-json-linuxx64:${properties["ktor-client-json.version"]}")
            }
        }

        val linuxX64Test by sourceSets.getting {
            dependsOn(commonTest)
            dependsOn(linuxX64Main)
        }

        val macosX64Main by sourceSets.getting {
            dependsOn(commonMain)
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json-macosx64:${properties["kotlinx.serialization.json.version"]}")
                implementation("com.soywiz.korlibs.korio:korio-macosx64:${properties["korlibs.version"]}")
                implementation("com.soywiz.korlibs.korgw:korgw-macosx64:${properties["korlibs.version"]}")
                implementation("io.ktor:ktor-client-json-macosx64:${properties["ktor-client-json.version"]}")
            }
        }

        val macosX64Test by sourceSets.getting {
            dependsOn(commonTest)
            dependsOn(macosX64Main)
        }

//            dependencies {
//                implementation(kotlin("stdlib-common"))
////                implementation("com.github.animeshz:mouse-kt-mingwX64:0.3.3")
//            }
//        }
//        val nativeMain by getting {
//            dependencies {
//                implementation(kotlin("stdlib-common"))
////                implementation("com.github.animeshz:mouse-kt-mingwX64:0.3.3")
//            }
//        }
//        val nativeTest by getting {
//            dependencies {
//                implementation(kotlin("stdlib-common"))
////                implementation("com.github.animeshz:keyboard-kt:0.3.3")
////                implementation("com.github.animeshz:mouse-kt:0.3.3")
//            }
//        }
    }
}
