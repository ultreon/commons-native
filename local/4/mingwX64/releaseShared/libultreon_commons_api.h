#ifndef KONAN_LIBULTREON_COMMONS_H
#define KONAN_LIBULTREON_COMMONS_H
#ifdef __cplusplus
extern "C" {
#endif
#ifdef __cplusplus
typedef bool            libultreon_commons_KBoolean;
#else
typedef _Bool           libultreon_commons_KBoolean;
#endif
typedef unsigned short     libultreon_commons_KChar;
typedef signed char        libultreon_commons_KByte;
typedef short              libultreon_commons_KShort;
typedef int                libultreon_commons_KInt;
typedef long long          libultreon_commons_KLong;
typedef unsigned char      libultreon_commons_KUByte;
typedef unsigned short     libultreon_commons_KUShort;
typedef unsigned int       libultreon_commons_KUInt;
typedef unsigned long long libultreon_commons_KULong;
typedef float              libultreon_commons_KFloat;
typedef double             libultreon_commons_KDouble;
#ifndef _MSC_VER
typedef float __attribute__ ((__vector_size__ (16))) libultreon_commons_KVector128;
#else
#include <xmmintrin.h>
typedef __m128 libultreon_commons_KVector128;
#endif
typedef void*              libultreon_commons_KNativePtr;
struct libultreon_commons_KType;
typedef struct libultreon_commons_KType libultreon_commons_KType;

typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Byte;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Short;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Int;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Long;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Float;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Double;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Char;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Boolean;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Unit;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Array;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_qboi_test_win32_Main;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_CrashHandler;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Throwable;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_collections_ArrayList;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Any;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashHandler;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_collections_List;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_devices_Win32Screen;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Color;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_internal_SerializationConstructorMarker;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Color_$serializer;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_descriptors_SerialDescriptor;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_encoding_Decoder;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_encoding_Encoder;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Color_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_KSerializer;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_DummyMessenger;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Function1;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel_$serializer;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_weather_OpenWeatherMap;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_weather_openweathermap_Weather;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_weather_OpenWeatherMap_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_weather_TempCelsius;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_weather_TempFahrenheit;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_weather_TempKelvin;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_json_JsonObject;


typedef struct {
  /* Service functions. */
  void (*DisposeStablePointer)(libultreon_commons_KNativePtr ptr);
  void (*DisposeString)(const char* string);
  libultreon_commons_KBoolean (*IsInstance)(libultreon_commons_KNativePtr ref, const libultreon_commons_KType* type);
  libultreon_commons_kref_kotlin_Byte (*createNullableByte)(libultreon_commons_KByte);
  libultreon_commons_kref_kotlin_Short (*createNullableShort)(libultreon_commons_KShort);
  libultreon_commons_kref_kotlin_Int (*createNullableInt)(libultreon_commons_KInt);
  libultreon_commons_kref_kotlin_Long (*createNullableLong)(libultreon_commons_KLong);
  libultreon_commons_kref_kotlin_Float (*createNullableFloat)(libultreon_commons_KFloat);
  libultreon_commons_kref_kotlin_Double (*createNullableDouble)(libultreon_commons_KDouble);
  libultreon_commons_kref_kotlin_Char (*createNullableChar)(libultreon_commons_KChar);
  libultreon_commons_kref_kotlin_Boolean (*createNullableBoolean)(libultreon_commons_KBoolean);
  libultreon_commons_kref_kotlin_Unit (*createNullableUnit)(void);

  /* User functions. */
  struct {
    struct {
      void (*main)();
      struct {
        struct {
          struct {
            struct {
              void (*main)(libultreon_commons_kref_kotlin_Array args);
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_qboi_test_win32_Main (*Main)();
                void (*doStuff)(libultreon_commons_kref_tk_qboi_test_win32_Main thiz);
              } Main;
            } win32;
          } test;
        } qboi;
        struct {
          struct {
            struct {
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash (*ApplicationCrash)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog crashLog);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*get_crashLog)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*getCrashLog)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash thiz);
                void (*onCrash)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash thiz, libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_CrashHandler handler);
                void (*printCrash)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_Companion (*_instance)();
                } Companion;
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  void (*run)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_CrashHandler thiz);
                } CrashHandler;
              } ApplicationCrash;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory (*CrashCategory)(const char* details);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory (*CrashCategory_)(const char* details, libultreon_commons_kref_kotlin_Throwable throwable);
                const char* (*get_details)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                libultreon_commons_kref_kotlin_collections_ArrayList (*get_entries)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                libultreon_commons_kref_kotlin_Throwable (*get_throwable)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                void (*set_throwable)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz, libultreon_commons_kref_kotlin_Throwable set);
                void (*add)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz, const char* key, libultreon_commons_kref_kotlin_Any value);
                const char* (*getDetails)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                libultreon_commons_kref_kotlin_Throwable (*getThrowable)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry (*CrashEntry)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz, const char* key, const char* value);
                  const char* (*get_key)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz);
                  const char* (*get_value)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz);
                  libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz, libultreon_commons_kref_kotlin_Any other);
                  libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz);
                  const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz);
                } CrashEntry;
              } CrashCategory;
              struct {
                libultreon_commons_KType* (*_type)(void);
                void (*handle)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashHandler thiz);
              } CrashHandler;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*CrashLog)(const char* details, libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog report);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*CrashLog_)(const char* details, libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog report, libultreon_commons_kref_kotlin_Throwable t);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*CrashLog__)(const char* details, libultreon_commons_kref_kotlin_Throwable t);
                void (*addCategory)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz, libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory crashCategory);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash (*createCrash)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                void (*defaultSave)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                libultreon_commons_kref_kotlin_collections_List (*getCategories)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                const char* (*getDefaultFileName)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                libultreon_commons_kref_kotlin_Throwable (*getThrowable)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
              } CrashLog;
            } crash;
            struct {
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode (*DisplayMode)(libultreon_commons_KInt width, libultreon_commons_KInt height, libultreon_commons_KInt bitsPerPixel);
                libultreon_commons_KInt (*get_bitsPerPixel)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz);
                libultreon_commons_KInt (*get_height)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz);
                libultreon_commons_KInt (*get_width)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz);
                libultreon_commons_KInt (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz);
                libultreon_commons_KInt (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz);
                libultreon_commons_KInt (*component3)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode (*copy)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz, libultreon_commons_KInt width, libultreon_commons_KInt height, libultreon_commons_KInt bitsPerPixel);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_devices_DisplayMode thiz);
              } DisplayMode;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_devices_Win32Screen (*Win32Screen)();
                libultreon_commons_KInt (*get_height)(libultreon_commons_kref_tk_ultreonteam_commons_devices_Win32Screen thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_screenSize)(libultreon_commons_kref_tk_ultreonteam_commons_devices_Win32Screen thiz);
                libultreon_commons_KInt (*get_width)(libultreon_commons_kref_tk_ultreonteam_commons_devices_Win32Screen thiz);
                libultreon_commons_KInt (*getDpi)(libultreon_commons_kref_tk_ultreonteam_commons_devices_Win32Screen thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel (*getPixel)(libultreon_commons_kref_tk_ultreonteam_commons_devices_Win32Screen thiz, libultreon_commons_KInt x, libultreon_commons_KInt y);
                void (*setPixel)(libultreon_commons_kref_tk_ultreonteam_commons_devices_Win32Screen thiz, libultreon_commons_KInt x, libultreon_commons_KInt y, libultreon_commons_kref_tk_ultreonteam_commons_lang_Color color);
              } Win32Screen;
            } devices;
            struct {
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug (*Debug)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug (*Debug_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug (*Debug__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug (*Debug___)(libultreon_commons_kref_kotlin_Throwable cause);
              } Debug;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException (*DuplicateElementException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException (*DuplicateElementException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException (*DuplicateElementException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException (*DuplicateElementException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } DuplicateElementException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException (*IllegalCallerException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException (*IllegalCallerException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException (*IllegalCallerException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException (*IllegalCallerException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } IllegalCallerException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException (*InvalidOrderException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException (*InvalidOrderException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException (*InvalidOrderException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException (*InvalidOrderException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } InvalidOrderException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException (*InvalidPlatformException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException (*InvalidPlatformException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException (*InvalidPlatformException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException (*InvalidPlatformException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } InvalidPlatformException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException (*InvalidValueException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException (*InvalidValueException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException (*InvalidValueException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException (*InvalidValueException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } InvalidValueException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException (*OneTimeUseException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException (*OneTimeUseException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException (*OneTimeUseException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException (*OneTimeUseException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } OneTimeUseException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException (*ReadonlyException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException (*ReadonlyException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException (*ReadonlyException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException (*ReadonlyException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } ReadonlyException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException (*RenderException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException (*RenderException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException (*RenderException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException (*RenderException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } RenderException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException (*SyntaxException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException (*SyntaxException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException (*SyntaxException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException (*SyntaxException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } SyntaxException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException (*SystemException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException (*SystemException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException (*SystemException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException (*SystemException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } SystemException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException (*UnsafeOperationException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException (*UnsafeOperationException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException (*UnsafeOperationException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException (*UnsafeOperationException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } UnsafeOperationException;
            } exceptions;
            struct {
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color)(libultreon_commons_KInt red, libultreon_commons_KInt green, libultreon_commons_KInt blue, libultreon_commons_KInt alpha);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color_)(const char* hex);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color__)(libultreon_commons_KInt red, libultreon_commons_KInt green, libultreon_commons_KInt blue);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color___)(libultreon_commons_KFloat red, libultreon_commons_KFloat green, libultreon_commons_KFloat blue, libultreon_commons_KFloat alpha);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color____)(libultreon_commons_KFloat red, libultreon_commons_KFloat green, libultreon_commons_KFloat blue);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color_____)(libultreon_commons_KInt rgb, libultreon_commons_KBoolean alpha, libultreon_commons_KBoolean reverse);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color______)(libultreon_commons_KUInt rgb, libultreon_commons_KBoolean alpha, libultreon_commons_KBoolean reverse);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color_______)(libultreon_commons_KInt seen1, libultreon_commons_KInt red, libultreon_commons_KInt green, libultreon_commons_KInt blue, libultreon_commons_KInt alpha, libultreon_commons_kref_kotlinx_serialization_internal_SerializationConstructorMarker serializationConstructorMarker);
                libultreon_commons_KInt (*get_alpha)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_blue)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_green)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_red)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_rgb)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_rgbReversed)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_rgba)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_rgbaReversed)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*brighter)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat amount);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*darker)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat amount);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                const char* (*toHex)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KBoolean withAlpha);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withAlpha)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat alpha);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withAlpha_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KInt alpha);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withBlue)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat blue);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withBlue_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KInt blue);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withGreen)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat green);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withGreen_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KInt green);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withRed)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat red);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withRed_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KInt red);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Color_$serializer (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_descriptors_SerialDescriptor (*get_descriptor)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color_$serializer thiz);
                  libultreon_commons_kref_kotlin_Array (*childSerializers)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color_$serializer thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*deserialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Decoder decoder);
                  void (*serialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Encoder encoder, libultreon_commons_kref_tk_ultreonteam_commons_lang_Color value);
                } $serializer;
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Color_Companion (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_KSerializer (*serializer)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color_Companion thiz);
                } Companion;
              } Color;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_DummyMessenger (*DummyMessenger)();
              } DummyMessenger;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger (*Messenger)(libultreon_commons_kref_kotlin_Function1 consumer);
                void (*send)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger thiz, const char* message);
              } Messenger;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage (*Percentage)(libultreon_commons_KInt seen1, libultreon_commons_KDouble percentage, libultreon_commons_kref_kotlinx_serialization_internal_SerializationConstructorMarker serializationConstructorMarker);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage (*Percentage_)(libultreon_commons_KDouble percentage);
                libultreon_commons_KDouble (*get_percentage)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz);
                void (*set_percentage)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz, libultreon_commons_KDouble set);
                libultreon_commons_KDouble (*get_value)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz);
                void (*set_value)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz, libultreon_commons_KDouble value);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_descriptors_SerialDescriptor (*get_descriptor)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer thiz);
                  libultreon_commons_kref_kotlin_Array (*childSerializers)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage (*deserialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Decoder decoder);
                  void (*serialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Encoder encoder, libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage value);
                } $serializer;
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_Companion (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_KSerializer (*serializer)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage (*toPercentage)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_Companion thiz, libultreon_commons_KDouble value);
                } Companion;
              } Percentage;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel (*Pixel)(libultreon_commons_KInt seen1, libultreon_commons_KInt x, libultreon_commons_KInt y, libultreon_commons_kref_tk_ultreonteam_commons_lang_Color color, libultreon_commons_kref_kotlinx_serialization_internal_SerializationConstructorMarker serializationConstructorMarker);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel (*Pixel_)(libultreon_commons_KInt x, libultreon_commons_KInt y, libultreon_commons_kref_tk_ultreonteam_commons_lang_Color color);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*get_color)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_KInt (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_KInt (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_KInt (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_KInt (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*component3)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel (*copy)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz, libultreon_commons_KInt x, libultreon_commons_KInt y, libultreon_commons_kref_tk_ultreonteam_commons_lang_Color color);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel_$serializer (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_descriptors_SerialDescriptor (*get_descriptor)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel_$serializer thiz);
                  libultreon_commons_kref_kotlin_Array (*childSerializers)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel_$serializer thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel (*deserialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Decoder decoder);
                  void (*serialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Encoder encoder, libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel value);
                } $serializer;
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel_Companion (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_KSerializer (*serializer)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel_Companion thiz);
                } Companion;
              } Pixel;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*Progress)(libultreon_commons_KInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*Progress_)(libultreon_commons_KInt progress, libultreon_commons_KInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*Progress__)(libultreon_commons_KInt seen1, libultreon_commons_KInt progress, libultreon_commons_KInt max, libultreon_commons_kref_kotlinx_serialization_internal_SerializationConstructorMarker serializationConstructorMarker);
                libultreon_commons_KInt (*get_max)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KFloat (*get_percentage)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KInt (*get_progress)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KFloat (*get_relativeProgress)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KInt (*get_todo)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KInt (*compareTo)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz, libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress other);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*copy)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                void (*increment)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz, libultreon_commons_KInt amount);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_descriptors_SerialDescriptor (*get_descriptor)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer thiz);
                  libultreon_commons_kref_kotlin_Array (*childSerializers)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*deserialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Decoder decoder);
                  void (*serialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Encoder encoder, libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress value);
                } $serializer;
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_Companion (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_KSerializer (*serializer)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_Companion thiz);
                } Companion;
              } Progress;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger (*ProgressMessenger)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger messenger, libultreon_commons_KInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger (*ProgressMessenger_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger messenger, libultreon_commons_KInt progress, libultreon_commons_KInt max);
                void (*send)(libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger thiz, const char* text);
                void (*sendNext)(libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger thiz, const char* text);
              } ProgressMessenger;
            } lang;
            struct {
              struct {
                libultreon_commons_KUInt (*toColorRef)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
              } color;
            } mingw;
            struct {
              libultreon_commons_KInt (*findBreakAfter)(const char* line, libultreon_commons_KInt start);
              libultreon_commons_KInt (*findBreakBefore)(const char* line, libultreon_commons_KInt start);
              libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*hex2Rgb)(const char* hex);
              libultreon_commons_kref_kotlin_Array (*multiConvertHexToRgb)(libultreon_commons_kref_kotlin_Array colorStrings);
              libultreon_commons_kref_kotlin_Array (*parseColorString)(const char* colorString);
              libultreon_commons_kref_kotlin_Array (*parseColorString_)(const char* colorString, libultreon_commons_KBoolean addPrefix);
              libultreon_commons_kref_kotlin_collections_List (*splitIntoLines)(const char* str);
              libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*unpackHex)(const char* hex);
              libultreon_commons_KInt (*count)(const char* thiz, libultreon_commons_KChar c);
            } util;
            struct {
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*Vec2b)(libultreon_commons_KByte x, libultreon_commons_KByte y);
                libultreon_commons_KByte (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte set);
                libultreon_commons_KByte (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte set);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*abs)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte min, libultreon_commons_KByte max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b min, libultreon_commons_KByte max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b max);
                libultreon_commons_KByte (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_KByte (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_KByte (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_KByte (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KByte (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_KByte (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_KByte (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KInt index, libultreon_commons_KByte value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_KByte other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*unaryMinus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*unaryPlus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_DOWN)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_DOWN_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_DOWN_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_UP)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_UP_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_UP_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b_Companion thiz);
                } Companion;
              } Vec2b;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*Vec2d)(libultreon_commons_KDouble x, libultreon_commons_KDouble y);
                libultreon_commons_KDouble (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble set);
                libultreon_commons_KDouble (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble set);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*abs)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble min, libultreon_commons_KDouble max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d min, libultreon_commons_KDouble max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d max);
                libultreon_commons_KDouble (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_KDouble (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_KDouble (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_KDouble (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KDouble (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_KDouble (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_KDouble (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KInt index, libultreon_commons_KDouble value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_KDouble other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*unaryMinus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*unaryPlus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_DOWN)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_DOWN_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_DOWN_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_HALF)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_UP)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_UP_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_UP_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d_Companion thiz);
                } Companion;
              } Vec2d;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*Vec2f)(libultreon_commons_KFloat x, libultreon_commons_KFloat y);
                libultreon_commons_KFloat (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat set);
                libultreon_commons_KFloat (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat set);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*abs)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat min, libultreon_commons_KFloat max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f min, libultreon_commons_KFloat max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f max);
                libultreon_commons_KFloat (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_KFloat (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_KFloat (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_KFloat (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KFloat (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_KFloat (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_KFloat (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KInt index, libultreon_commons_KFloat value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_KFloat other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*unaryMinus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*unaryPlus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_DOWN)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_DOWN_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_DOWN_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_HALF)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_UP)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_UP_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_UP_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f_Companion thiz);
                } Companion;
              } Vec2f;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*Vec2i)(libultreon_commons_KInt x, libultreon_commons_KInt y);
                libultreon_commons_KInt (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt set);
                libultreon_commons_KInt (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt set);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*abs)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt min, libultreon_commons_KInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i min, libultreon_commons_KInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i max);
                libultreon_commons_KInt (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_KInt (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_KInt (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_KInt (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KInt (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_KInt (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_KInt (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt index, libultreon_commons_KInt value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_KInt other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*unaryMinus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*unaryPlus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_DOWN)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_DOWN_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_DOWN_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_UP)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_UP_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_UP_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i_Companion thiz);
                } Companion;
              } Vec2i;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*Vec2l)(libultreon_commons_KLong x, libultreon_commons_KLong y);
                libultreon_commons_KLong (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong set);
                libultreon_commons_KLong (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong set);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*abs)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong min, libultreon_commons_KLong max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l min, libultreon_commons_KLong max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l max);
                libultreon_commons_KLong (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_KLong (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_KLong (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_KLong (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KLong (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_KLong (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_KLong (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KInt index, libultreon_commons_KLong value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_KLong other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*unaryMinus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*unaryPlus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_DOWN)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_DOWN_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_DOWN_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_UP)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_UP_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_UP_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l_Companion thiz);
                } Companion;
              } Vec2l;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*Vec2s)(libultreon_commons_KShort x, libultreon_commons_KShort y);
                libultreon_commons_KShort (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort set);
                libultreon_commons_KShort (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort set);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*abs)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort min, libultreon_commons_KShort max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s min, libultreon_commons_KShort max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s max);
                libultreon_commons_KShort (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_KShort (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_KShort (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_KShort (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KShort (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_KShort (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_KShort (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KInt index, libultreon_commons_KShort value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_KShort other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*unaryMinus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*unaryPlus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_DOWN)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_DOWN_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_DOWN_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_UP)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_UP_LEFT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_UP_RIGHT)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s_Companion thiz);
                } Companion;
              } Vec2s;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*Vec2u)(libultreon_commons_KUInt x, libultreon_commons_KUInt y);
                libultreon_commons_KUInt (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt set);
                libultreon_commons_KUInt (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt set);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt min, libultreon_commons_KUInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u min, libultreon_commons_KUInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u max);
                libultreon_commons_KUInt (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_KUInt (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_KUInt (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_KUInt (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KUInt (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_KUInt (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_KUInt (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KInt index, libultreon_commons_KUInt value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_KUInt other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u_Companion thiz);
                } Companion;
              } Vec2u;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*Vec2ub)(libultreon_commons_KUByte x, libultreon_commons_KUByte y);
                libultreon_commons_KUByte (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte set);
                libultreon_commons_KUByte (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte set);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte min, libultreon_commons_KUByte max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub min, libultreon_commons_KUByte max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub max);
                libultreon_commons_KUByte (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_KUByte (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_KUByte (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_KUByte (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KUByte (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_KUByte (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_KUByte (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KInt index, libultreon_commons_KUByte value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_KUByte other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub_Companion thiz);
                } Companion;
              } Vec2ub;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*Vec2ul)(libultreon_commons_KULong x, libultreon_commons_KULong y);
                libultreon_commons_KULong (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong set);
                libultreon_commons_KULong (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong set);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong min, libultreon_commons_KULong max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul min, libultreon_commons_KULong max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul max);
                libultreon_commons_KULong (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_KULong (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_KULong (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_KULong (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KULong (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_KULong (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_KULong (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KInt index, libultreon_commons_KULong value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_KULong other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul_Companion thiz);
                } Companion;
              } Vec2ul;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*Vec2us)(libultreon_commons_KUShort x, libultreon_commons_KUShort y);
                libultreon_commons_KUShort (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                void (*set_x)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort set);
                libultreon_commons_KUShort (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                void (*set_y)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort set);
                libultreon_commons_KDouble (*angle)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_KDouble (*angle_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*clamp)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort min, libultreon_commons_KUShort max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*clamp_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*clamp__)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us min, libultreon_commons_KUShort max);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*clamp___)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us min, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us max);
                libultreon_commons_KUShort (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_KUShort (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_KUShort (*cross)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*div)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*div_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                void (*divAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort other);
                void (*divAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_KUShort (*dot)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KUShort (*get)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KInt index);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_KUShort (*length)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_KUShort (*lengthSquared)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*max)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*max_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*min)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*min_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*minus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                void (*minusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*normalize)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*plus)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                void (*plusAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*rem)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*rem_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                void (*remAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort other);
                void (*remAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*rotate)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KDouble angle);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*rotate_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KFloat angle);
                void (*set)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KInt index, libultreon_commons_KUShort value);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*times)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*times_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                void (*timesAssign)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_KUShort other);
                void (*timesAssign_)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz, libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us other);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2b (*toVec2b)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2d (*toVec2d)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2f (*toVec2f)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2i (*toVec2i)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2l (*toVec2l)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2s (*toVec2s)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2u (*toVec2u)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ub (*toVec2ub)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2ul (*toVec2ul)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*toVec2us)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us_Companion (*_instance)();
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*get_ONE)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us (*get_ZERO)(libultreon_commons_kref_tk_ultreonteam_commons_vector_Vec2us_Companion thiz);
                } Companion;
              } Vec2us;
            } vector;
            struct {
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_weather_OpenWeatherMap (*OpenWeatherMap)(const char* apiKey);
                libultreon_commons_kref_tk_ultreonteam_commons_weather_openweathermap_Weather (*getWeatherSync)(libultreon_commons_kref_tk_ultreonteam_commons_weather_OpenWeatherMap thiz, const char* city);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_weather_OpenWeatherMap_Companion (*_instance)();
                  const char* (*get_API_URL)(libultreon_commons_kref_tk_ultreonteam_commons_weather_OpenWeatherMap_Companion thiz);
                } Companion;
              } OpenWeatherMap;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_weather_TempCelsius (*TempCelsius)(libultreon_commons_KDouble value);
                libultreon_commons_KDouble (*get_fahrenheit)(libultreon_commons_kref_tk_ultreonteam_commons_weather_TempCelsius thiz);
                libultreon_commons_KDouble (*get_kelvin)(libultreon_commons_kref_tk_ultreonteam_commons_weather_TempCelsius thiz);
                libultreon_commons_KDouble (*get_value)(libultreon_commons_kref_tk_ultreonteam_commons_weather_TempCelsius thiz);
              } TempCelsius;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_weather_TempFahrenheit (*TempFahrenheit)(libultreon_commons_KDouble value);
                libultreon_commons_KDouble (*get_celsius)(libultreon_commons_kref_tk_ultreonteam_commons_weather_TempFahrenheit thiz);
                libultreon_commons_KDouble (*get_kelvin)(libultreon_commons_kref_tk_ultreonteam_commons_weather_TempFahrenheit thiz);
                libultreon_commons_KDouble (*get_value)(libultreon_commons_kref_tk_ultreonteam_commons_weather_TempFahrenheit thiz);
              } TempFahrenheit;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_weather_TempKelvin (*TempKelvin)(libultreon_commons_KDouble value);
                libultreon_commons_KDouble (*get_celcius)(libultreon_commons_kref_tk_ultreonteam_commons_weather_TempKelvin thiz);
                libultreon_commons_KDouble (*get_fahrenheit)(libultreon_commons_kref_tk_ultreonteam_commons_weather_TempKelvin thiz);
                libultreon_commons_KDouble (*get_value)(libultreon_commons_kref_tk_ultreonteam_commons_weather_TempKelvin thiz);
              } TempKelvin;
              struct {
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_weather_openweathermap_Weather (*Weather)(libultreon_commons_kref_kotlinx_serialization_json_JsonObject data);
                } Weather;
              } openweathermap;
            } weather;
          } commons;
        } ultreonteam;
      } tk;
    } root;
  } kotlin;
} libultreon_commons_ExportedSymbols;
extern libultreon_commons_ExportedSymbols* libultreon_commons_symbols(void);
#ifdef __cplusplus
}  /* extern "C" */
#endif
#endif  /* KONAN_LIBULTREON_COMMONS_H */
