#ifndef KONAN_LIBULTREON_COMMONS_H
#define KONAN_LIBULTREON_COMMONS_H
#ifdef __cplusplus
extern "C" {
#endif
#ifdef __cplusplus
typedef bool            libultreon_commons_KBoolean;
#else
typedef _Bool           libultreon_commons_KBoolean;
#endif
typedef unsigned short     libultreon_commons_KChar;
typedef signed char        libultreon_commons_KByte;
typedef short              libultreon_commons_KShort;
typedef int                libultreon_commons_KInt;
typedef long long          libultreon_commons_KLong;
typedef unsigned char      libultreon_commons_KUByte;
typedef unsigned short     libultreon_commons_KUShort;
typedef unsigned int       libultreon_commons_KUInt;
typedef unsigned long long libultreon_commons_KULong;
typedef float              libultreon_commons_KFloat;
typedef double             libultreon_commons_KDouble;
#ifndef _MSC_VER
typedef float __attribute__ ((__vector_size__ (16))) libultreon_commons_KVector128;
#else
#include <xmmintrin.h>
typedef __m128 libultreon_commons_KVector128;
#endif
typedef void*              libultreon_commons_KNativePtr;
struct libultreon_commons_KType;
typedef struct libultreon_commons_KType libultreon_commons_KType;

typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Byte;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Short;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Int;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Long;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Float;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Double;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Char;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Boolean;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Unit;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Array;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_qboi_test_win32_Main;


typedef struct {
  /* Service functions. */
  void (*DisposeStablePointer)(libultreon_commons_KNativePtr ptr);
  void (*DisposeString)(const char* string);
  libultreon_commons_KBoolean (*IsInstance)(libultreon_commons_KNativePtr ref, const libultreon_commons_KType* type);
  libultreon_commons_kref_kotlin_Byte (*createNullableByte)(libultreon_commons_KByte);
  libultreon_commons_kref_kotlin_Short (*createNullableShort)(libultreon_commons_KShort);
  libultreon_commons_kref_kotlin_Int (*createNullableInt)(libultreon_commons_KInt);
  libultreon_commons_kref_kotlin_Long (*createNullableLong)(libultreon_commons_KLong);
  libultreon_commons_kref_kotlin_Float (*createNullableFloat)(libultreon_commons_KFloat);
  libultreon_commons_kref_kotlin_Double (*createNullableDouble)(libultreon_commons_KDouble);
  libultreon_commons_kref_kotlin_Char (*createNullableChar)(libultreon_commons_KChar);
  libultreon_commons_kref_kotlin_Boolean (*createNullableBoolean)(libultreon_commons_KBoolean);
  libultreon_commons_kref_kotlin_Unit (*createNullableUnit)(void);

  /* User functions. */
  struct {
    struct {
      void (*main)();
      struct {
        struct {
          struct {
            struct {
              void (*main)(libultreon_commons_kref_kotlin_Array args);
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_qboi_test_win32_Main (*Main)();
                void (*doStuff)(libultreon_commons_kref_tk_qboi_test_win32_Main thiz);
              } Main;
            } win32;
          } test;
        } qboi;
      } tk;
    } root;
  } kotlin;
} libultreon_commons_ExportedSymbols;
extern libultreon_commons_ExportedSymbols* libultreon_commons_symbols(void);
#ifdef __cplusplus
}  /* extern "C" */
#endif
#endif  /* KONAN_LIBULTREON_COMMONS_H */
