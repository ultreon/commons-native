#ifndef KONAN_LIBULTREON_COMMONS_H
#define KONAN_LIBULTREON_COMMONS_H
#ifdef __cplusplus
extern "C" {
#endif
#ifdef __cplusplus
typedef bool            libultreon_commons_KBoolean;
#else
typedef _Bool           libultreon_commons_KBoolean;
#endif
typedef unsigned short     libultreon_commons_KChar;
typedef signed char        libultreon_commons_KByte;
typedef short              libultreon_commons_KShort;
typedef int                libultreon_commons_KInt;
typedef long long          libultreon_commons_KLong;
typedef unsigned char      libultreon_commons_KUByte;
typedef unsigned short     libultreon_commons_KUShort;
typedef unsigned int       libultreon_commons_KUInt;
typedef unsigned long long libultreon_commons_KULong;
typedef float              libultreon_commons_KFloat;
typedef double             libultreon_commons_KDouble;
#ifndef _MSC_VER
typedef float __attribute__ ((__vector_size__ (16))) libultreon_commons_KVector128;
#else
#include <xmmintrin.h>
typedef __m128 libultreon_commons_KVector128;
#endif
typedef void*              libultreon_commons_KNativePtr;
struct libultreon_commons_KType;
typedef struct libultreon_commons_KType libultreon_commons_KType;

typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Byte;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Short;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Int;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Long;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Float;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Double;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Char;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Boolean;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Unit;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Array;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_qboi_test_win32_Main;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_CrashHandler;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Throwable;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_collections_ArrayList;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Any;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashHandler;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_collections_List;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Color;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_DummyMessenger;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlin_Function1;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_internal_SerializationConstructorMarker;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_descriptors_SerialDescriptor;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_encoding_Decoder;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_encoding_Encoder;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_kotlinx_serialization_KSerializer;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_Companion;
typedef struct {
  libultreon_commons_KNativePtr pinned;
} libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger;


typedef struct {
  /* Service functions. */
  void (*DisposeStablePointer)(libultreon_commons_KNativePtr ptr);
  void (*DisposeString)(const char* string);
  libultreon_commons_KBoolean (*IsInstance)(libultreon_commons_KNativePtr ref, const libultreon_commons_KType* type);
  libultreon_commons_kref_kotlin_Byte (*createNullableByte)(libultreon_commons_KByte);
  libultreon_commons_kref_kotlin_Short (*createNullableShort)(libultreon_commons_KShort);
  libultreon_commons_kref_kotlin_Int (*createNullableInt)(libultreon_commons_KInt);
  libultreon_commons_kref_kotlin_Long (*createNullableLong)(libultreon_commons_KLong);
  libultreon_commons_kref_kotlin_Float (*createNullableFloat)(libultreon_commons_KFloat);
  libultreon_commons_kref_kotlin_Double (*createNullableDouble)(libultreon_commons_KDouble);
  libultreon_commons_kref_kotlin_Char (*createNullableChar)(libultreon_commons_KChar);
  libultreon_commons_kref_kotlin_Boolean (*createNullableBoolean)(libultreon_commons_KBoolean);
  libultreon_commons_kref_kotlin_Unit (*createNullableUnit)(void);

  /* User functions. */
  struct {
    struct {
      void (*main)();
      struct {
        struct {
          struct {
            struct {
              void (*main)(libultreon_commons_kref_kotlin_Array args);
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_qboi_test_win32_Main (*Main)();
                void (*doStuff)(libultreon_commons_kref_tk_qboi_test_win32_Main thiz);
              } Main;
            } win32;
          } test;
        } qboi;
        struct {
          struct {
            struct {
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash (*ApplicationCrash)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog crashLog);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*get_crashLog)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*getCrashLog)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash thiz);
                void (*onCrash)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash thiz, libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_CrashHandler handler);
                void (*printCrash)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_Companion (*_instance)();
                } Companion;
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  void (*run)(libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash_CrashHandler thiz);
                } CrashHandler;
              } ApplicationCrash;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory (*CrashCategory)(const char* details);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory (*CrashCategory_)(const char* details, libultreon_commons_kref_kotlin_Throwable throwable);
                const char* (*get_details)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                libultreon_commons_kref_kotlin_collections_ArrayList (*get_entries)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                libultreon_commons_kref_kotlin_Throwable (*get_throwable)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                void (*set_throwable)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz, libultreon_commons_kref_kotlin_Throwable set);
                void (*add)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz, const char* key, libultreon_commons_kref_kotlin_Any value);
                const char* (*getDetails)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                libultreon_commons_kref_kotlin_Throwable (*getThrowable)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry (*CrashEntry)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory thiz, const char* key, const char* value);
                  const char* (*get_key)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz);
                  const char* (*get_value)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz);
                  libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz, libultreon_commons_kref_kotlin_Any other);
                  libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz);
                  const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory_CrashEntry thiz);
                } CrashEntry;
              } CrashCategory;
              struct {
                libultreon_commons_KType* (*_type)(void);
                void (*handle)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashHandler thiz);
              } CrashHandler;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*CrashLog)(const char* details, libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog report);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*CrashLog_)(const char* details, libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog report, libultreon_commons_kref_kotlin_Throwable t);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog (*CrashLog__)(const char* details, libultreon_commons_kref_kotlin_Throwable t);
                void (*addCategory)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz, libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashCategory crashCategory);
                libultreon_commons_kref_tk_ultreonteam_commons_crash_ApplicationCrash (*createCrash)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                void (*defaultSave)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                libultreon_commons_kref_kotlin_collections_List (*getCategories)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                const char* (*getDefaultFileName)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                libultreon_commons_kref_kotlin_Throwable (*getThrowable)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_crash_CrashLog thiz);
              } CrashLog;
            } crash;
            struct {
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug (*Debug)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug (*Debug_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug (*Debug__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_Debug (*Debug___)(libultreon_commons_kref_kotlin_Throwable cause);
              } Debug;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException (*DuplicateElementException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException (*DuplicateElementException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException (*DuplicateElementException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_DuplicateElementException (*DuplicateElementException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } DuplicateElementException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException (*IllegalCallerException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException (*IllegalCallerException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException (*IllegalCallerException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_IllegalCallerException (*IllegalCallerException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } IllegalCallerException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException (*InvalidOrderException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException (*InvalidOrderException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException (*InvalidOrderException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidOrderException (*InvalidOrderException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } InvalidOrderException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException (*InvalidPlatformException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException (*InvalidPlatformException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException (*InvalidPlatformException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidPlatformException (*InvalidPlatformException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } InvalidPlatformException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException (*InvalidValueException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException (*InvalidValueException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException (*InvalidValueException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_InvalidValueException (*InvalidValueException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } InvalidValueException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException (*OneTimeUseException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException (*OneTimeUseException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException (*OneTimeUseException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_OneTimeUseException (*OneTimeUseException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } OneTimeUseException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException (*ReadonlyException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException (*ReadonlyException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException (*ReadonlyException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_ReadonlyException (*ReadonlyException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } ReadonlyException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException (*RenderException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException (*RenderException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException (*RenderException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_RenderException (*RenderException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } RenderException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException (*SyntaxException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException (*SyntaxException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException (*SyntaxException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SyntaxException (*SyntaxException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } SyntaxException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException (*SystemException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException (*SystemException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException (*SystemException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_SystemException (*SystemException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } SystemException;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException (*UnsafeOperationException)();
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException (*UnsafeOperationException_)(const char* message);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException (*UnsafeOperationException__)(const char* message, libultreon_commons_kref_kotlin_Throwable cause);
                libultreon_commons_kref_tk_ultreonteam_commons_exceptions_UnsafeOperationException (*UnsafeOperationException___)(libultreon_commons_kref_kotlin_Throwable cause);
              } UnsafeOperationException;
            } exceptions;
            struct {
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color)(libultreon_commons_KInt red, libultreon_commons_KInt green, libultreon_commons_KInt blue, libultreon_commons_KInt alpha);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color_)(const char* hex);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color__)(libultreon_commons_KInt red, libultreon_commons_KInt green, libultreon_commons_KInt blue);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color___)(libultreon_commons_KFloat red, libultreon_commons_KFloat green, libultreon_commons_KFloat blue, libultreon_commons_KFloat alpha);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color____)(libultreon_commons_KFloat red, libultreon_commons_KFloat green, libultreon_commons_KFloat blue);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*Color_____)(libultreon_commons_KInt rgb, libultreon_commons_KBoolean alpha);
                libultreon_commons_KInt (*get_alpha)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_blue)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_green)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_KInt (*get_red)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*brighter)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat amount);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*darker)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat amount);
                const char* (*toHex)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KBoolean withAlpha);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withAlpha)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat alpha);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withAlpha_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KInt alpha);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withBlue)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat blue);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withBlue_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KInt blue);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withGreen)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat green);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withGreen_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KInt green);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withRed)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KFloat red);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*withRed_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Color thiz, libultreon_commons_KInt red);
              } Color;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_DummyMessenger (*DummyMessenger)();
              } DummyMessenger;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger (*Messenger)(libultreon_commons_kref_kotlin_Function1 consumer);
                void (*send)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger thiz, const char* message);
              } Messenger;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage (*Percentage)(libultreon_commons_KInt seen1, libultreon_commons_KDouble percentage, libultreon_commons_kref_kotlinx_serialization_internal_SerializationConstructorMarker serializationConstructorMarker);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage (*Percentage_)(libultreon_commons_KDouble percentage);
                libultreon_commons_KDouble (*get_percentage)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz);
                void (*set_percentage)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz, libultreon_commons_KDouble set);
                libultreon_commons_KDouble (*get_value)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz);
                void (*set_value)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz, libultreon_commons_KDouble value);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_descriptors_SerialDescriptor (*get_descriptor)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer thiz);
                  libultreon_commons_kref_kotlin_Array (*childSerializers)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage (*deserialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Decoder decoder);
                  void (*serialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Encoder encoder, libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage value);
                } $serializer;
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_Companion (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_KSerializer (*serializer)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_Companion thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage (*toPercentage)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Percentage_Companion thiz, libultreon_commons_KDouble value);
                } Companion;
              } Percentage;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel (*Pixel)(libultreon_commons_KInt x, libultreon_commons_KInt y, libultreon_commons_kref_tk_ultreonteam_commons_lang_Color color);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*get_color)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_KInt (*get_x)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_KInt (*get_y)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_KInt (*component1)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_KInt (*component2)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*component3)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel (*copy)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz, libultreon_commons_KInt x, libultreon_commons_KInt y, libultreon_commons_kref_tk_ultreonteam_commons_lang_Color color);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Pixel thiz);
              } Pixel;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*Progress)(libultreon_commons_KInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*Progress_)(libultreon_commons_KInt progress, libultreon_commons_KInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*Progress__)(libultreon_commons_KInt seen1, libultreon_commons_KInt progress, libultreon_commons_KInt max, libultreon_commons_kref_kotlinx_serialization_internal_SerializationConstructorMarker serializationConstructorMarker);
                libultreon_commons_KInt (*get_max)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KFloat (*get_percentage)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KInt (*get_progress)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KFloat (*get_relativeProgress)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KInt (*get_todo)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KInt (*compareTo)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz, libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress other);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*copy)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                libultreon_commons_KBoolean (*equals)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz, libultreon_commons_kref_kotlin_Any other);
                libultreon_commons_KInt (*hashCode)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                void (*increment)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz, libultreon_commons_KInt amount);
                const char* (*toString)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress thiz);
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_descriptors_SerialDescriptor (*get_descriptor)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer thiz);
                  libultreon_commons_kref_kotlin_Array (*childSerializers)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer thiz);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress (*deserialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Decoder decoder);
                  void (*serialize)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_$serializer thiz, libultreon_commons_kref_kotlinx_serialization_encoding_Encoder encoder, libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress value);
                } $serializer;
                struct {
                  libultreon_commons_KType* (*_type)(void);
                  libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_Companion (*_instance)();
                  libultreon_commons_kref_kotlinx_serialization_KSerializer (*serializer)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Progress_Companion thiz);
                } Companion;
              } Progress;
              struct {
                libultreon_commons_KType* (*_type)(void);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger (*ProgressMessenger)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger messenger, libultreon_commons_KInt max);
                libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger (*ProgressMessenger_)(libultreon_commons_kref_tk_ultreonteam_commons_lang_Messenger messenger, libultreon_commons_KInt progress, libultreon_commons_KInt max);
                void (*send)(libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger thiz, const char* text);
                void (*sendNext)(libultreon_commons_kref_tk_ultreonteam_commons_lang_ProgressMessenger thiz, const char* text);
              } ProgressMessenger;
            } lang;
            struct {
              libultreon_commons_KInt (*findBreakAfter)(const char* line, libultreon_commons_KInt start);
              libultreon_commons_KInt (*findBreakBefore)(const char* line, libultreon_commons_KInt start);
              libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*hex2Rgb)(const char* hex);
              libultreon_commons_kref_kotlin_Array (*multiConvertHexToRgb)(libultreon_commons_kref_kotlin_Array colorStrings);
              libultreon_commons_kref_kotlin_Array (*parseColorString)(const char* colorString);
              libultreon_commons_kref_kotlin_Array (*parseColorString_)(const char* colorString, libultreon_commons_KBoolean addPrefix);
              libultreon_commons_kref_kotlin_collections_List (*splitIntoLines)(const char* str);
              libultreon_commons_kref_tk_ultreonteam_commons_lang_Color (*unpackHex)(const char* hex);
              libultreon_commons_KInt (*count)(const char* thiz, libultreon_commons_KChar c);
            } util;
          } commons;
        } ultreonteam;
      } tk;
    } root;
  } kotlin;
} libultreon_commons_ExportedSymbols;
extern libultreon_commons_ExportedSymbols* libultreon_commons_symbols(void);
#ifdef __cplusplus
}  /* extern "C" */
#endif
#endif  /* KONAN_LIBULTREON_COMMONS_H */
