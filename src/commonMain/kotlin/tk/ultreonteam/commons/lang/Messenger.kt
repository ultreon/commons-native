package tk.ultreonteam.commons.lang

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
open class Messenger(private val consumer: (String) -> Unit) {
    fun send(message: String) {
        consumer(message)
    }
}