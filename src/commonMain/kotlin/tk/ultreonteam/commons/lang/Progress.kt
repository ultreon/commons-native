package tk.ultreonteam.commons.lang

import kotlinx.cinterop.internal.CStruct
import kotlinx.serialization.Serializable

@Serializable
@CStruct.CPlusPlusClass
open class Progress : Comparable<Progress> {
    var progress: Int
        private set
    var max: Int
        private set

    val percentage: Float
        get() = 100f * progress / max

    val relativeProgress: Float
        get() = progress.toFloat() / max

    val todo: Int
        get() = (max - progress).coerceAtLeast(0)

    constructor(max: Int) : this(0, max)

    constructor(progress: Int, max: Int) {
        this.progress = progress
        this.max = max
    }

    fun increment(amount: Int = 1) {
        if (progress + 1 <= max) progress += amount else error("Progress increment at end: ${progress+1}, max: $max")
    }

    override fun compareTo(other: Progress): Int {
        return progress.compareTo(other.progress)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        if (this::class != other::class) return false

        other as Progress

        if (progress != other.progress) return false
        if (max != other.max) return false

        return true
    }

    override fun hashCode(): Int {
        var result = progress.hashCode()
        result = 31 * result + max.hashCode()
        return result
    }

    override fun toString(): String {
        return "Progress(progress=$progress, max=$max)"
    }

    fun copy(): Progress {
        return Progress(progress, max)
    }
}