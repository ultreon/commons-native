package tk.ultreonteam.commons.lang

import kotlinx.cinterop.internal.CStruct
import kotlinx.serialization.Serializable

@CStruct.CPlusPlusClass
@Serializable
class Color {
    val red: Int
    val green: Int
    val blue: Int
    val alpha: Int
    val rgb: Int
        get() = (red shl 16) or (green shl 8) or blue
    val rgbReversed: Int
        get() = (blue shl 16) or (green shl 8) or red
    val rgba: Int
        get() = alpha shl 24 or rgb
    val rgbaReversed: Int
        get() = alpha shl 24 or rgbReversed

    constructor(red: Int, green: Int, blue: Int, alpha: Int) {
        this.red = red
        this.green = green
        this.blue = blue
        this.alpha = alpha
    }

    constructor(hex: String) {
        var parsingHex = hex
        if (hex.startsWith("#")) {
            parsingHex = parsingHex.substring(1)
        } else if (hex.startsWith("0x")) {
            parsingHex = parsingHex.substring(2)
        }

        when (hex.length) {
            3, 4 -> {
                parsingHex = hex.map { it -> "$it$it" }.joinToString(separator = "")
            }
            6, 8 -> {
                // do nothing
            }
            else -> {
                throw IllegalArgumentException("Invalid hex color string: $hex")
            }
        }

        red = parsingHex.substring(0, 2).toInt(radix = 16)
        green = parsingHex.substring(2, 4).toInt(radix = 16)
        blue = parsingHex.substring(4, 6).toInt(radix = 16)
        alpha = if (hex.length == 8) parsingHex.substring(6, 8).toInt(radix = 16) else 255
    }

    constructor(red: Int, green: Int, blue: Int) : this(red, green, blue, 255)

    constructor(red: Float, green: Float, blue: Float, alpha: Float) : this(
        (red * 255).toInt(),
        (green * 255).toInt(),
        (blue * 255).toInt(),
        (alpha * 255).toInt()
    )

    constructor(red: Float, green: Float, blue: Float) : this(
        (red * 255).toInt(),
        (green * 255).toInt(),
        (blue * 255).toInt()
    )

    constructor(rgb: Int, alpha: Boolean = false, reverse: Boolean = false) {
        if (reverse) {
            blue = rgb shr 16 and 0xFF
            green = rgb shr 8 and 0xFF
            red = rgb and 0xFF
        } else {
            red = rgb shr 16 and 0xFF
            green = rgb shr 8 and 0xFF
            blue = rgb and 0xFF
        }

        if (alpha) {
            this.alpha = rgb shr 24 and 0xFF
        } else {
            this.alpha = 255
        }
    }

    constructor(rgb: UInt, alpha: Boolean = false, reverse: Boolean = false) : this(rgb.toInt(), alpha, reverse)

    fun toHex(withAlpha: Boolean = true): String {
        return ("#" +
                red.toString(radix = 16).padStart(2, '0') +
                green.toString(radix = 16).padStart(2, '0') +
                blue.toString(radix = 16).padStart(2, '0')).also {
                    if (withAlpha) it + alpha.toString(radix = 16).padStart(2, '0') else it
                }
    }

    fun brighter(amount: Float = 0.2f): Color {
        val r = (red + 255 * amount).toInt()
        val g = (green + 255 * amount).toInt()
        val b = (blue + 255 * amount).toInt()
        val a = (alpha + 255 * amount).toInt()
        return Color(r, g, b, a)
    }

    fun darker(amount: Float = 0.2f): Color {
        val r = (red - 255 * amount).toInt()
        val g = (green - 255 * amount).toInt()
        val b = (blue - 255 * amount).toInt()
        val a = (alpha - 255 * amount).toInt()
        return Color(r, g, b, a)
    }

    fun withAlpha(alpha: Int): Color {
        return Color(red, green, blue, alpha)
    }

    fun withAlpha(alpha: Float): Color {
        return Color(red, green, blue, (alpha * 255).toInt())
    }

    fun withRed(red: Int): Color {
        return Color(red, green, blue, alpha)
    }

    fun withRed(red: Float): Color {
        return Color((red * 255).toInt(), green, blue, alpha)
    }

    fun withGreen(green: Int): Color {
        return Color(red, green, blue, alpha)
    }

    fun withGreen(green: Float): Color {
        return Color(red, (green * 255).toInt(), blue, alpha)
    }

    fun withBlue(blue: Int): Color {
        return Color(red, green, blue, alpha)
    }

    fun withBlue(blue: Float): Color {
        return Color(red, green, (blue * 255).toInt(), alpha)
    }

    override fun toString(): String {
        return "Color(red=$red, green=$green, blue=$blue, alpha=$alpha)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        if (this::class != other::class) return false

        other as Color

        if (red != other.red) return false
        if (green != other.green) return false
        if (blue != other.blue) return false
        if (alpha != other.alpha) return false

        return true
    }

    override fun hashCode(): Int {
        var result = red.hashCode()
        result = 31 * result + green.hashCode()
        result = 31 * result + blue.hashCode()
        result = 31 * result + alpha.hashCode()
        return result
    }
}
