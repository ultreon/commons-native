package tk.ultreonteam.commons.lang

import kotlinx.cinterop.internal.CStruct
import kotlinx.serialization.Serializable

@CStruct.CPlusPlusClass
@Serializable
data class Pixel(val x: Int, val y: Int, val color: Color) {
    override fun toString(): String {
        return "Pixel(x=$x, y=$y, color=${color.toHex()})"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        if (this::class != other::class) return false

        other as Pixel

        if (x != other.x) return false
        if (y != other.y) return false
        if (color != other.color) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        result = 31 * result + color.hashCode()
        return result
    }

}