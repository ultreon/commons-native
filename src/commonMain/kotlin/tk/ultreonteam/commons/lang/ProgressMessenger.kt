package tk.ultreonteam.commons.lang

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
open class ProgressMessenger : Progress {
    private var messenger: Messenger

    constructor(messenger: Messenger, max: Int) : super(max) {
        this.messenger = messenger
    }

    constructor(messenger: Messenger, progress: Int, max: Int) : super(progress, max) {
        this.messenger = messenger
    }

    fun send(text: String) {
        messenger.send(text)
    }

    fun sendNext(text: String) {
        send(text)
        increment()
    }
}