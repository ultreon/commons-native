package tk.ultreonteam.commons.lang

import kotlinx.serialization.Serializable

@Serializable
class Percentage(var percentage: Double) {
    var value: Double
        get() = percentage / 100
        set(value) {
            percentage = value * 100
        }

    companion object {
        fun toPercentage(value: Double): Percentage {
            return Percentage(value * 100)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null) return false
        if (this::class != other::class) return false

        other as Percentage

        if (percentage != other.percentage) return false

        return true
    }

    override fun hashCode(): Int {
        return percentage.hashCode()
    }

    override fun toString(): String {
        return "Percentage(percentage=$percentage)"
    }
}