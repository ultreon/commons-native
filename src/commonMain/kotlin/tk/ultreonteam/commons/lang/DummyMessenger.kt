package tk.ultreonteam.commons.lang

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class DummyMessenger : Messenger({})