package tk.ultreonteam.commons.vector

import kotlin.math.cos
import kotlin.math.sin

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Vec2ul(var x: ULong, var y: ULong) {
    companion object {
        val ZERO = Vec2ul(0uL, 0uL)
        val ONE = Vec2ul(1uL, 1uL)
    }

    operator fun plus(other: Vec2ul): Vec2ul = Vec2ul(x + other.x, y + other.y)
    operator fun minus(other: Vec2ul): Vec2ul = Vec2ul(x - other.x, y - other.y)
    operator fun times(other: Vec2ul): Vec2ul = Vec2ul(x * other.x, y * other.y)
    operator fun rem(other: Vec2ul): Vec2ul = Vec2ul(x % other.x, y % other.y)
    operator fun div(other: Vec2ul): Vec2ul = Vec2ul(x / other.x, y / other.y)
    operator fun times(other: ULong): Vec2ul = Vec2ul(x * other, y * other)
    operator fun div(other: ULong): Vec2ul = Vec2ul(x / other, y / other)
    operator fun rem(other: ULong): Vec2ul = Vec2ul(x % other, y % other)
    operator fun plusAssign(other: Vec2ul) {
        x += other.x
        y += other.y
    }
    operator fun minusAssign(other: Vec2ul) {
        x -= other.x
        y -= other.y
    }
    operator fun timesAssign(other: Vec2ul) {
        x *= other.x
        y *= other.y
    }
    operator fun divAssign(other: Vec2ul) {
        x /= other.x
        y /= other.y
    }
    operator fun remAssign(other: Vec2ul) {
        x %= other.x
        y %= other.y
    }
    operator fun timesAssign(other: ULong) {
        x *= other
        y *= other
    }
    operator fun divAssign(other: ULong) {
        x /= other
        y /= other
    }
    operator fun remAssign(other: ULong) {
        x %= other
        y %= other
    }
    operator fun get(index: Int): ULong = when (index) {
        0 -> x
        1 -> y
        else -> throw IndexOutOfBoundsException()
    }
    operator fun set(index: Int, value: ULong): Unit = when (index) {
        0 -> x = value
        1 -> y = value
        else -> throw IndexOutOfBoundsException()
    }

    fun max(other: Vec2ul): Vec2ul = Vec2ul(kotlin.math.max(x, other.x), kotlin.math.max(y, other.y))
    fun min(other: Vec2ul): Vec2ul = Vec2ul(kotlin.math.min(x, other.x), kotlin.math.min(y, other.y))
    fun max(other: ULong): Vec2ul = Vec2ul(kotlin.math.max(x, other), kotlin.math.max(y, other))
    fun min(other: ULong): Vec2ul = Vec2ul(kotlin.math.min(x, other), kotlin.math.min(y, other))
    fun clamp(min: Vec2ul, max: Vec2ul): Vec2ul = Vec2ul(kotlin.math.max(min.x, kotlin.math.min(max.x, x)), kotlin.math.max(min.y, kotlin.math.min(max.y, y)))
    fun clamp(min: ULong, max: ULong): Vec2ul = Vec2ul(kotlin.math.max(min, kotlin.math.min(max, x)), kotlin.math.max(min, kotlin.math.min(max, y)))
    fun clamp(min: ULong, max: Vec2ul): Vec2ul = Vec2ul(kotlin.math.max(min, kotlin.math.min(max.x, x)), kotlin.math.max(min, kotlin.math.min(max.y, y)))
    fun clamp(min: Vec2ul, max: ULong): Vec2ul = Vec2ul(kotlin.math.max(min.x, kotlin.math.min(max, x)), kotlin.math.max(min.y, kotlin.math.min(max, y)))
    fun cross(other: Vec2ul): ULong = x * other.y - y * other.x
    fun dot(other: Vec2ul): ULong = x * other.x + y * other.y
    fun length(): ULong = kotlin.math.sqrt(x.toDouble() * x.toDouble() + y.toDouble() * y.toDouble().toDouble()).toULong()
    fun lengthSquared(): ULong = x * x + y * y
    fun normalize(): Vec2ul = this / length()
    fun rotate(angle: Double): Vec2ul = Vec2ul((x.toDouble() * cos(angle) - y.toDouble() * sin(angle)).toULong(), (x.toDouble() * sin(angle) + y.toDouble() * cos(angle)).toULong())
    fun rotate(angle: Float): Vec2ul = Vec2ul((x.toDouble() * cos(angle) - y.toDouble() * sin(angle)).toULong(), (x.toDouble() * sin(angle) + y.toDouble() * cos(angle)).toULong())
    fun angle(): Double = kotlin.math.atan2(y.toDouble(), x.toDouble())
    fun angle(other: Vec2ul): Double = kotlin.math.atan2(y.toDouble(), x.toDouble()) - kotlin.math.atan2(other.y.toDouble(), other.x.toDouble())
    
    fun toVec2f(): Vec2f = Vec2f(x.toFloat(), y.toFloat())
    fun toVec2d(): Vec2d = Vec2d(x.toDouble(), y.toDouble())
    fun toVec2b(): Vec2b = Vec2b(x.toByte(), y.toByte())
    fun toVec2s(): Vec2s = Vec2s(x.toShort(), y.toShort())
    fun toVec2i(): Vec2i = Vec2i(x.toInt(), y.toInt())
    fun toVec2l(): Vec2l = Vec2l(x.toLong(), y.toLong())
    fun toVec2ub(): Vec2ub = Vec2ub(x.toUByte(), y.toUByte())
    fun toVec2us(): Vec2us = Vec2us(x.toUShort(), y.toUShort())
    fun toVec2u(): Vec2u = Vec2u(x.toUInt(), y.toUInt())
    fun toVec2ul(): Vec2ul = Vec2ul(x, y)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vec2ul) return false

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    operator fun component1(): ULong = x
    operator fun component2(): ULong = y
}
