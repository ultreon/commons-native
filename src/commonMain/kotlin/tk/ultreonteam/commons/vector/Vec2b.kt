package tk.ultreonteam.commons.vector

import kotlinx.cinterop.internal.CStruct
import kotlin.math.cos
import kotlin.math.sin


@CStruct.CPlusPlusClass
class Vec2b (var x: Byte, var y: Byte) {
    companion object {
        val ZERO = Vec2b(0, 0)
        val ONE = Vec2b(1, 1)
        val UP = Vec2b(0, -1)
        val DOWN = Vec2b(0, 1)
        val LEFT = Vec2b(-1, 0)
        val RIGHT = Vec2b(1, 0)
        val UP_LEFT = Vec2b(-1, -1)
        val UP_RIGHT = Vec2b(1, -1)
        val DOWN_LEFT = Vec2b(-1, 1)
        val DOWN_RIGHT = Vec2b(1, 1)
    }

    operator fun plus(other: Vec2b): Vec2b = Vec2b((x + other.x).toByte(), (y + other.y).toByte())
    operator fun minus(other: Vec2b): Vec2b = Vec2b((x - other.x).toByte(), (y - other.y).toByte())
    operator fun times(other: Vec2b): Vec2b = Vec2b((x * other.x).toByte(), (y * other.y).toByte())
    operator fun rem(other: Vec2b): Vec2b = Vec2b((x % other.x).toByte(), (y % other.y).toByte())
    operator fun div(other: Vec2b): Vec2b = Vec2b((x / other.x).toByte(), (y / other.y).toByte())
    operator fun times(other: Byte): Vec2b = Vec2b((x * other).toByte(), (y * other).toByte())
    operator fun div(other: Byte): Vec2b = Vec2b((x / other).toByte(), (y / other).toByte())
    operator fun rem(other: Byte): Vec2b = Vec2b((x % other).toByte(), (y % other).toByte())
    operator fun plusAssign(other: Vec2b) {
        x = (x + other.x).toByte()
        y = (y + other.y).toByte()
    }
    operator fun minusAssign(other: Vec2b) {
        x = (x -other.x).toByte()
        y = (y -other.y).toByte()
    }
    operator fun timesAssign(other: Vec2b) {
        x = (x * other.x).toByte()
        y = (y * other.y).toByte()
    }
    operator fun divAssign(other: Vec2b) {
        x = (x / other.x).toByte()
        y = (y / other.y).toByte()
    }
    operator fun remAssign(other: Vec2b) {
        x = (x % other.x).toByte()
        y = (y % other.y).toByte()
    }
    operator fun timesAssign(other: Byte) {
        x = (x * other).toByte()
        y = (y * other).toByte()
    }
    operator fun divAssign(other: Byte) {
        x = (x / other).toByte()
        y = (x / other).toByte()
    }
    operator fun remAssign(other: Byte) {
        x = (x % other).toByte()
        y = (y % other).toByte()
    }
    operator fun unaryMinus(): Vec2b = Vec2b((-(x.toInt())).toByte(), (-(y.toInt())).toByte())
    operator fun unaryPlus(): Vec2b = Vec2b((+(x.toInt())).toByte(), (+(y.toInt())).toByte())
    operator fun get(index: Int): Byte = when (index) {
        0 -> x
        1 -> y
        else -> throw IndexOutOfBoundsException()
    }
    operator fun set(index: Int, value: Byte): Unit = when (index) {
        0 -> x = value
        1 -> y = value
        else -> throw IndexOutOfBoundsException()
    }

    fun abs(): Vec2b = Vec2b(kotlin.math.abs(x.toInt()).toByte(), kotlin.math.abs(y.toInt()).toByte())
    fun max(other: Vec2b): Vec2b = Vec2b(kotlin.math.max(x.toInt(), other.x.toInt()).toByte(), kotlin.math.max(y.toInt(), other.y.toInt()).toByte())
    fun min(other: Vec2b): Vec2b = Vec2b(kotlin.math.min(x.toInt(), other.x.toInt()).toByte(), kotlin.math.min(y.toInt(), other.y.toInt()).toByte())
    fun max(other: Byte): Vec2b = Vec2b(kotlin.math.max(x.toInt(), other.toInt()).toByte(), kotlin.math.max(y.toInt(), other.toInt()).toByte())
    fun min(other: Byte): Vec2b = Vec2b(kotlin.math.min(x.toInt(), other.toInt()).toByte(), kotlin.math.min(y.toInt(), other.toInt()).toByte())
    fun clamp(min: Vec2b, max: Vec2b): Vec2b = Vec2b(kotlin.math.max(min.x.toInt(), kotlin.math.min(max.x.toInt(), x.toInt())).toByte(), kotlin.math.max(min.y.toInt(), kotlin.math.min(max.y.toInt(), y.toInt())).toByte())
    fun clamp(min: Byte, max: Byte): Vec2b = Vec2b(kotlin.math.max(min.toInt(), kotlin.math.min(max.toInt(), x.toInt())).toByte(), kotlin.math.max(min.toInt(), kotlin.math.min(max.toInt(), y.toInt())).toByte())
    fun clamp(min: Byte, max: Vec2b): Vec2b = Vec2b(kotlin.math.max(min.toInt(), kotlin.math.min(max.x.toInt(), x.toInt())).toByte(), kotlin.math.max(min.toInt(), kotlin.math.min(max.y.toInt(), y.toInt())).toByte())
    fun clamp(min: Vec2b, max: Byte): Vec2b = Vec2b(kotlin.math.max(min.x.toInt(), kotlin.math.min(max.toInt(), x.toInt())).toByte(), kotlin.math.max(min.y.toInt(), kotlin.math.min(max.toInt(), y.toInt())).toByte())
    fun cross(other: Vec2b): Byte = (x * other.y - y * other.x).toByte()
    fun dot(other: Vec2b): Byte = (x * other.x + y * other.y).toByte()
    fun length(): Byte = kotlin.math.sqrt(x * x + y * y.toDouble()).toInt().toByte()
    fun lengthSquared(): Byte = (x * x + y * y).toByte()
    fun normalize(): Vec2b = this / length()
    fun rotate(angle: Double): Vec2b = Vec2b((x * cos(angle) - y * sin(angle)).toInt().toByte(), (x * sin(angle) + y * cos(angle)).toInt().toByte())
    fun rotate(angle: Float): Vec2b = Vec2b((x * cos(angle) - y * sin(angle)).toInt().toByte(), (x * sin(angle) + y * cos(angle)).toInt().toByte())
    fun angle(): Double = kotlin.math.atan2(y.toDouble(), x.toDouble())
    fun angle(other: Vec2b): Double = kotlin.math.atan2(y.toDouble(), x.toDouble()) - kotlin.math.atan2(other.y.toDouble(), other.x.toDouble())



    fun toVec2f(): Vec2f = Vec2f(x.toFloat(), y.toFloat())
    fun toVec2d(): Vec2d = Vec2d(x.toDouble(), y.toDouble())
    fun toVec2b(): Vec2b = Vec2b(x, y)
    fun toVec2s(): Vec2s = Vec2s(x.toShort(), y.toShort())
    fun toVec2i(): Vec2i = Vec2i(x.toInt(), y.toInt())
    fun toVec2l(): Vec2l = Vec2l(x.toLong(), y.toLong())
    fun toVec2ub(): Vec2ub = Vec2ub(x.toUByte(), y.toUByte())
    fun toVec2us(): Vec2us = Vec2us(x.toUShort(), y.toUShort())
    fun toVec2u(): Vec2u = Vec2u(x.toUInt(), y.toUInt())
    fun toVec2ul(): Vec2ul = Vec2ul(x.toULong(), y.toULong())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vec2b) return false

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    operator fun component1(): Byte = x
    operator fun component2(): Byte = y
}
