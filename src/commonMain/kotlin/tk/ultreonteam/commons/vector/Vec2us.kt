package tk.ultreonteam.commons.vector

import kotlin.math.cos
import kotlin.math.sin

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Vec2us(var x: UShort, var y: UShort) {
    companion object {
        val ZERO = Vec2us(0u, 0u)
        val ONE = Vec2us(1u, 1u)
    }

    operator fun plus(other: Vec2us): Vec2us = Vec2us((x + other.x).toUShort(), (y + other.y).toUShort())
    operator fun minus(other: Vec2us): Vec2us = Vec2us((x - other.x).toUShort(), (y - other.y).toUShort())
    operator fun times(other: Vec2us): Vec2us = Vec2us((x * other.x).toUShort(), (y * other.y).toUShort())
    operator fun rem(other: Vec2us): Vec2us = Vec2us((x % other.x).toUShort(), (y % other.y).toUShort())
    operator fun div(other: Vec2us): Vec2us = Vec2us((x / other.x).toUShort(), (y / other.y).toUShort())
    operator fun times(other: UShort): Vec2us = Vec2us((x * other).toUShort(), (y * other).toUShort())
    operator fun div(other: UShort): Vec2us = Vec2us((x / other).toUShort(), (y / other).toUShort())
    operator fun rem(other: UShort): Vec2us = Vec2us((x % other).toUShort(), (y % other).toUShort())
    operator fun plusAssign(other: Vec2us) {
        x = (x + other.x).toUShort()
        y = (y + other.y).toUShort()
    }
    operator fun minusAssign(other: Vec2us) {
        x = (x -other.x).toUShort()
        y = (y -other.y).toUShort()
    }
    operator fun timesAssign(other: Vec2us) {
        x = (x * other.x).toUShort()
        y = (y * other.y).toUShort()
    }
    operator fun divAssign(other: Vec2us) {
        x = (x / other.x).toUShort()
        y = (y / other.y).toUShort()
    }
    operator fun remAssign(other: Vec2us) {
        x = (x % other.x).toUShort()
        y = (y % other.y).toUShort()
    }
    operator fun timesAssign(other: UShort) {
        x = (x * other).toUShort()
        y = (y * other).toUShort()
    }
    operator fun divAssign(other: UShort) {
        x = (x / other).toUShort()
        y = (x / other).toUShort()
    }
    operator fun remAssign(other: UShort) {
        x = (x % other).toUShort()
        y = (y % other).toUShort()
    }
    operator fun get(index: Int): UShort = when (index) {
        0 -> x
        1 -> y
        else -> throw IndexOutOfBoundsException()
    }
    operator fun set(index: Int, value: UShort): Unit = when (index) {
        0 -> x = value
        1 -> y = value
        else -> throw IndexOutOfBoundsException()
    }

    fun max(other: Vec2us): Vec2us = Vec2us(kotlin.math.max(x.toInt(), other.x.toInt()).toUShort(), kotlin.math.max(y.toInt(), other.y.toInt()).toUShort())
    fun min(other: Vec2us): Vec2us = Vec2us(kotlin.math.min(x.toInt(), other.x.toInt()).toUShort(), kotlin.math.min(y.toInt(), other.y.toInt()).toUShort())
    fun max(other: UShort): Vec2us = Vec2us(kotlin.math.max(x.toInt(), other.toInt()).toUShort(), kotlin.math.max(y.toInt(), other.toInt()).toUShort())
    fun min(other: UShort): Vec2us = Vec2us(kotlin.math.min(x.toInt(), other.toInt()).toUShort(), kotlin.math.min(y.toInt(), other.toInt()).toUShort())
    fun clamp(min: Vec2us, max: Vec2us): Vec2us = Vec2us(kotlin.math.max(min.x.toInt(), kotlin.math.min(max.x.toInt(), x.toInt())).toUShort(), kotlin.math.max(min.y.toInt(), kotlin.math.min(max.y.toInt(), y.toInt())).toUShort())
    fun clamp(min: UShort, max: UShort): Vec2us = Vec2us(kotlin.math.max(min.toInt(), kotlin.math.min(max.toInt(), x.toInt())).toUShort(), kotlin.math.max(min.toInt(), kotlin.math.min(max.toInt(), y.toInt())).toUShort())
    fun clamp(min: UShort, max: Vec2us): Vec2us = Vec2us(kotlin.math.max(min.toInt(), kotlin.math.min(max.x.toInt(), x.toInt())).toUShort(), kotlin.math.max(min.toInt(), kotlin.math.min(max.y.toInt(), y.toInt())).toUShort())
    fun clamp(min: Vec2us, max: UShort): Vec2us = Vec2us(kotlin.math.max(min.x.toInt(), kotlin.math.min(max.toInt(), x.toInt())).toUShort(), kotlin.math.max(min.y.toInt(), kotlin.math.min(max.toInt(), y.toInt())).toUShort())
    fun cross(other: Vec2us): UShort = (x * other.y - y * other.x).toUShort()
    fun dot(other: Vec2us): UShort = (x * other.x + y * other.y).toUShort()
    fun length(): UShort = kotlin.math.sqrt(x.toDouble() * x.toDouble() + y.toDouble() * y.toDouble()).toInt().toUShort()
    fun lengthSquared(): UShort = (x * x + y * y).toUShort()
    fun normalize(): Vec2us = this / length()
    fun rotate(angle: Double): Vec2us = Vec2us((x.toDouble() * cos(angle) - y.toDouble() * sin(angle)).toInt().toUShort(), (x.toDouble() * sin(angle) + y.toDouble() * cos(angle)).toInt().toUShort())
    fun rotate(angle: Float): Vec2us = Vec2us((x.toDouble() * cos(angle) - y.toDouble() * sin(angle)).toInt().toUShort(), (x.toDouble() * sin(angle) + y.toDouble() * cos(angle)).toInt().toUShort())
    fun angle(): Double = kotlin.math.atan2(y.toDouble(), x.toDouble())
    fun angle(other: Vec2us): Double = kotlin.math.atan2(y.toDouble(), x.toDouble()) - kotlin.math.atan2(other.y.toDouble(), other.x.toDouble())
    
    fun toVec2f(): Vec2f = Vec2f(x.toFloat(), y.toFloat())
    fun toVec2d(): Vec2d = Vec2d(x.toDouble(), y.toDouble())
    fun toVec2b(): Vec2b = Vec2b(x.toByte(), y.toByte())
    fun toVec2s(): Vec2s = Vec2s(x.toShort(), y.toShort())
    fun toVec2i(): Vec2i = Vec2i(x.toInt(), y.toInt())
    fun toVec2l(): Vec2l = Vec2l(x.toLong(), y.toLong())
    fun toVec2ub(): Vec2ub = Vec2ub(x.toUByte(), y.toUByte())
    fun toVec2us(): Vec2us = Vec2us(x, y)
    fun toVec2u(): Vec2u = Vec2u(x.toUInt(), y.toUInt())
    fun toVec2ul(): Vec2ul = Vec2ul(x.toULong(), y.toULong())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vec2us) return false

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    operator fun component1(): UShort = x
    operator fun component2(): UShort = y
}
