package tk.ultreonteam.commons.vector

import kotlinx.cinterop.internal.CStruct
import kotlin.math.cos
import kotlin.math.sin


@CStruct.CPlusPlusClass
class Vec2f(var x: Float, var y: Float) {
    companion object {
        val ZERO = Vec2f(0f, 0f)
        val HALF = Vec2f(0.5f, 0.5f)
        val ONE = Vec2f(1f, 1f)
        val UP = Vec2f(0f, -1f)
        val DOWN = Vec2f(0f, 1f)
        val LEFT = Vec2f(-1f, 0f)
        val RIGHT = Vec2f(1f, 0f)
        val UP_LEFT = Vec2f(-1f, -1f)
        val UP_RIGHT = Vec2f(1f, -1f)
        val DOWN_LEFT = Vec2f(-1f, 1f)
        val DOWN_RIGHT = Vec2f(1f, 1f)
    }

    operator fun plus(other: Vec2f): Vec2f = Vec2f(x + other.x, y + other.y)
    operator fun minus(other: Vec2f): Vec2f = Vec2f(x - other.x, y - other.y)
    operator fun times(other: Vec2f): Vec2f = Vec2f(x * other.x, y * other.y)
    operator fun rem(other: Vec2f): Vec2f = Vec2f(x % other.x, y % other.y)
    operator fun div(other: Vec2f): Vec2f = Vec2f(x / other.x, y / other.y)
    operator fun times(other: Float): Vec2f = Vec2f(x * other, y * other)
    operator fun div(other: Float): Vec2f = Vec2f(x / other, y / other)
    operator fun rem(other: Float): Vec2f = Vec2f(x % other, y % other)
    operator fun plusAssign(other: Vec2f) {
        x += other.x
        y += other.y
    }
    operator fun minusAssign(other: Vec2f) {
        x -= other.x
        y -= other.y
    }
    operator fun timesAssign(other: Vec2f) {
        x *= other.x
        y *= other.y
    }
    operator fun divAssign(other: Vec2f) {
        x /= other.x
        y /= other.y
    }
    operator fun remAssign(other: Vec2f) {
        x %= other.x
        y %= other.y
    }
    operator fun timesAssign(other: Float) {
        x *= other
        y *= other
    }
    operator fun divAssign(other: Float) {
        x /= other
        y /= other
    }
    operator fun remAssign(other: Float) {
        x %= other
        y %= other
    }
    operator fun unaryMinus(): Vec2f = Vec2f(-x, -y)
    operator fun unaryPlus(): Vec2f = Vec2f(+x, +y)
    operator fun get(index: Int): Float = when (index) {
        0 -> x
        1 -> y
        else -> throw IndexOutOfBoundsException()
    }
    operator fun set(index: Int, value: Float): Unit = when (index) {
        0 -> x = value
        1 -> y = value
        else -> throw IndexOutOfBoundsException()
    }

    fun abs(): Vec2f = Vec2f(kotlin.math.abs(x), kotlin.math.abs(y))
    fun max(other: Vec2f): Vec2f = Vec2f(kotlin.math.max(x, other.x), kotlin.math.max(y, other.y))
    fun min(other: Vec2f): Vec2f = Vec2f(kotlin.math.min(x, other.x), kotlin.math.min(y, other.y))
    fun max(other: Float): Vec2f = Vec2f(kotlin.math.max(x, other), kotlin.math.max(y, other))
    fun min(other: Float): Vec2f = Vec2f(kotlin.math.min(x, other), kotlin.math.min(y, other))
    fun clamp(min: Vec2f, max: Vec2f): Vec2f = Vec2f(kotlin.math.max(min.x, kotlin.math.min(max.x, x)), kotlin.math.max(min.y, kotlin.math.min(max.y, y)))
    fun clamp(min: Float, max: Float): Vec2f = Vec2f(kotlin.math.max(min, kotlin.math.min(max, x)), kotlin.math.max(min, kotlin.math.min(max, y)))
    fun clamp(min: Float, max: Vec2f): Vec2f = Vec2f(kotlin.math.max(min, kotlin.math.min(max.x, x)), kotlin.math.max(min, kotlin.math.min(max.y, y)))
    fun clamp(min: Vec2f, max: Float): Vec2f = Vec2f(kotlin.math.max(min.x, kotlin.math.min(max, x)), kotlin.math.max(min.y, kotlin.math.min(max, y)))
    fun cross(other: Vec2f): Float = x * other.y - y * other.x
    fun dot(other: Vec2f): Float = x * other.x + y * other.y
    fun length(): Float = kotlin.math.sqrt(x * x + y * y)
    fun lengthSquared(): Float = x * x + y * y
    fun normalize(): Vec2f = this / length()
    fun rotate(angle: Double): Vec2f = Vec2f((x * cos(angle) - y * sin(angle)).toFloat(), (x * sin(angle) + y * cos(angle)).toFloat())
    fun rotate(angle: Float): Vec2f = Vec2f((x * cos(angle) - y * sin(angle)), (x * sin(angle) + y * cos(angle)))
    fun angle(): Double = kotlin.math.atan2(y.toDouble(), x.toDouble())
    fun angle(other: Vec2f): Double = kotlin.math.atan2(y.toDouble(), x.toDouble()) - kotlin.math.atan2(other.y.toDouble(), other.x.toDouble())



    fun toVec2f(): Vec2f = Vec2f(x, y)
    fun toVec2d(): Vec2d = Vec2d(x.toDouble(), y.toDouble())
    fun toVec2b(): Vec2b = Vec2b(x.toInt().toByte(), y.toInt().toByte())
    fun toVec2s(): Vec2s = Vec2s(x.toInt().toShort(), y.toInt().toShort())
    fun toVec2i(): Vec2i = Vec2i(x.toInt(), y.toInt())
    fun toVec2l(): Vec2l = Vec2l(x.toLong(), y.toLong())
    fun toVec2ub(): Vec2ub = Vec2ub(x.toInt().toUByte(), y.toInt().toUByte())
    fun toVec2us(): Vec2us = Vec2us(x.toInt().toUShort(), y.toInt().toUShort())
    fun toVec2u(): Vec2u = Vec2u(x.toUInt(), y.toUInt())
    fun toVec2ul(): Vec2ul = Vec2ul(x.toULong(), y.toULong())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vec2f) return false

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    operator fun component1(): Float = x
    operator fun component2(): Float = y
}
