package tk.ultreonteam.commons.vector

import kotlin.math.cos
import kotlin.math.sin

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Vec2ub(var x: UByte, var y: UByte) {
    companion object {
        val ZERO = Vec2ub(0u, 0u)
        val ONE = Vec2ub(1u, 1u)
    }

    operator fun plus(other: Vec2ub): Vec2ub = Vec2ub((x + other.x).toUByte(), (y + other.y).toUByte())
    operator fun minus(other: Vec2ub): Vec2ub = Vec2ub((x - other.x).toUByte(), (y - other.y).toUByte())
    operator fun times(other: Vec2ub): Vec2ub = Vec2ub((x * other.x).toUByte(), (y * other.y).toUByte())
    operator fun rem(other: Vec2ub): Vec2ub = Vec2ub((x % other.x).toUByte(), (y % other.y).toUByte())
    operator fun div(other: Vec2ub): Vec2ub = Vec2ub((x / other.x).toUByte(), (y / other.y).toUByte())
    operator fun times(other: UByte): Vec2ub = Vec2ub((x * other).toUByte(), (y * other).toUByte())
    operator fun div(other: UByte): Vec2ub = Vec2ub((x / other).toUByte(), (y / other).toUByte())
    operator fun rem(other: UByte): Vec2ub = Vec2ub((x % other).toUByte(), (y % other).toUByte())
    operator fun plusAssign(other: Vec2ub) {
        x = (x + other.x).toUByte()
        y = (y + other.y).toUByte()
    }
    operator fun minusAssign(other: Vec2ub) {
        x = (x -other.x).toUByte()
        y = (y -other.y).toUByte()
    }
    operator fun timesAssign(other: Vec2ub) {
        x = (x * other.x).toUByte()
        y = (y * other.y).toUByte()
    }
    operator fun divAssign(other: Vec2ub) {
        x = (x / other.x).toUByte()
        y = (y / other.y).toUByte()
    }
    operator fun remAssign(other: Vec2ub) {
        x = (x % other.x).toUByte()
        y = (y % other.y).toUByte()
    }
    operator fun timesAssign(other: UByte) {
        x = (x * other).toUByte()
        y = (y * other).toUByte()
    }
    operator fun divAssign(other: UByte) {
        x = (x / other).toUByte()
        y = (x / other).toUByte()
    }
    operator fun remAssign(other: UByte) {
        x = (x % other).toUByte()
        y = (y % other).toUByte()
    }
    operator fun get(index: Int): UByte = when (index) {
        0 -> x
        1 -> y
        else -> throw IndexOutOfBoundsException()
    }
    operator fun set(index: Int, value: UByte): Unit = when (index) {
        0 -> x = value
        1 -> y = value
        else -> throw IndexOutOfBoundsException()
    }

    fun max(other: Vec2ub): Vec2ub = Vec2ub(kotlin.math.max(x.toInt(), other.x.toInt()).toUByte(), kotlin.math.max(y.toInt(), other.y.toInt()).toUByte())
    fun min(other: Vec2ub): Vec2ub = Vec2ub(kotlin.math.min(x.toInt(), other.x.toInt()).toUByte(), kotlin.math.min(y.toInt(), other.y.toInt()).toUByte())
    fun max(other: UByte): Vec2ub = Vec2ub(kotlin.math.max(x.toInt(), other.toInt()).toUByte(), kotlin.math.max(y.toInt(), other.toInt()).toUByte())
    fun min(other: UByte): Vec2ub = Vec2ub(kotlin.math.min(x.toInt(), other.toInt()).toUByte(), kotlin.math.min(y.toInt(), other.toInt()).toUByte())
    fun clamp(min: Vec2ub, max: Vec2ub): Vec2ub = Vec2ub(kotlin.math.max(min.x.toInt(), kotlin.math.min(max.x.toInt(), x.toInt())).toUByte(), kotlin.math.max(min.y.toInt(), kotlin.math.min(max.y.toInt(), y.toInt())).toUByte())
    fun clamp(min: UByte, max: UByte): Vec2ub = Vec2ub(kotlin.math.max(min.toInt(), kotlin.math.min(max.toInt(), x.toInt())).toUByte(), kotlin.math.max(min.toInt(), kotlin.math.min(max.toInt(), y.toInt())).toUByte())
    fun clamp(min: UByte, max: Vec2ub): Vec2ub = Vec2ub(kotlin.math.max(min.toInt(), kotlin.math.min(max.x.toInt(), x.toInt())).toUByte(), kotlin.math.max(min.toInt(), kotlin.math.min(max.y.toInt(), y.toInt())).toUByte())
    fun clamp(min: Vec2ub, max: UByte): Vec2ub = Vec2ub(kotlin.math.max(min.x.toInt(), kotlin.math.min(max.toInt(), x.toInt())).toUByte(), kotlin.math.max(min.y.toInt(), kotlin.math.min(max.toInt(), y.toInt())).toUByte())
    fun cross(other: Vec2ub): UByte = (x * other.y - y * other.x).toUByte()
    fun dot(other: Vec2ub): UByte = (x * other.x + y * other.y).toUByte()
    fun length(): UByte = kotlin.math.sqrt(x.toDouble() * x.toDouble() + y.toDouble() * y.toDouble()).toInt().toUByte()
    fun lengthSquared(): UByte = (x * x + y * y).toUByte()
    fun normalize(): Vec2ub = this / length()
    fun rotate(angle: Double): Vec2ub = Vec2ub((x.toDouble() * cos(angle) - y.toDouble() * sin(angle)).toInt().toUByte(), (x.toDouble() * sin(angle) + y.toDouble() * cos(angle)).toInt().toUByte())
    fun rotate(angle: Float): Vec2ub = Vec2ub((x.toDouble() * cos(angle) - y.toDouble() * sin(angle)).toInt().toUByte(), (x.toDouble() * sin(angle) + y.toDouble() * cos(angle)).toInt().toUByte())
    fun angle(): Double = kotlin.math.atan2(y.toDouble(), x.toDouble())
    fun angle(other: Vec2ub): Double = kotlin.math.atan2(y.toDouble(), x.toDouble()) - kotlin.math.atan2(other.y.toDouble(), other.x.toDouble())
    
    fun toVec2f(): Vec2f = Vec2f(x.toFloat(), y.toFloat())
    fun toVec2d(): Vec2d = Vec2d(x.toDouble(), y.toDouble())
    fun toVec2b(): Vec2b = Vec2b(x.toByte(), y.toByte())
    fun toVec2s(): Vec2s = Vec2s(x.toShort(), y.toShort())
    fun toVec2i(): Vec2i = Vec2i(x.toInt(), y.toInt())
    fun toVec2l(): Vec2l = Vec2l(x.toLong(), y.toLong())
    fun toVec2ub(): Vec2ub = Vec2ub(x, y)
    fun toVec2us(): Vec2us = Vec2us(x.toUShort(), y.toUShort())
    fun toVec2u(): Vec2u = Vec2u(x.toUInt(), y.toUInt())
    fun toVec2ul(): Vec2ul = Vec2ul(x.toULong(), y.toULong())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vec2ub) return false

        other as Vec2ub

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    operator fun component1(): UByte = x
    operator fun component2(): UByte = y
}
