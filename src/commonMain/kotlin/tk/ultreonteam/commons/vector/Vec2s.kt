package tk.ultreonteam.commons.vector

import kotlin.math.cos
import kotlin.math.sin

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Vec2s(var x: Short, var y: Short) {
    companion object {
        val ZERO = Vec2s(0, 0)
        val ONE = Vec2s(1, 1)
        val UP = Vec2s(0, -1)
        val DOWN = Vec2s(0, 1)
        val LEFT = Vec2s(-1, 0)
        val RIGHT = Vec2s(1, 0)
        val UP_LEFT = Vec2s(-1, -1)
        val UP_RIGHT = Vec2s(1, -1)
        val DOWN_LEFT = Vec2s(-1, 1)
        val DOWN_RIGHT = Vec2s(1, 1)
    }

    operator fun plus(other: Vec2s): Vec2s = Vec2s((x + other.x).toShort(), (y + other.y).toShort())
    operator fun minus(other: Vec2s): Vec2s = Vec2s((x - other.x).toShort(), (y - other.y).toShort())
    operator fun times(other: Vec2s): Vec2s = Vec2s((x * other.x).toShort(), (y * other.y).toShort())
    operator fun rem(other: Vec2s): Vec2s = Vec2s((x % other.x).toShort(), (y % other.y).toShort())
    operator fun div(other: Vec2s): Vec2s = Vec2s((x / other.x).toShort(), (y / other.y).toShort())
    operator fun times(other: Short): Vec2s = Vec2s((x * other).toShort(), (y * other).toShort())
    operator fun div(other: Short): Vec2s = Vec2s((x / other).toShort(), (y / other).toShort())
    operator fun rem(other: Short): Vec2s = Vec2s((x % other).toShort(), (y % other).toShort())
    operator fun plusAssign(other: Vec2s) {
        x = (x + other.x).toShort()
        y = (y + other.y).toShort()
    }
    operator fun minusAssign(other: Vec2s) {
        x = (x -other.x).toShort()
        y = (y -other.y).toShort()
    }
    operator fun timesAssign(other: Vec2s) {
        x = (x * other.x).toShort()
        y = (y * other.y).toShort()
    }
    operator fun divAssign(other: Vec2s) {
        x = (x / other.x).toShort()
        y = (y / other.y).toShort()
    }
    operator fun remAssign(other: Vec2s) {
        x = (x % other.x).toShort()
        y = (y % other.y).toShort()
    }
    operator fun timesAssign(other: Short) {
        x = (x * other).toShort()
        y = (y * other).toShort()
    }
    operator fun divAssign(other: Short) {
        x = (x / other).toShort()
        y = (x / other).toShort()
    }
    operator fun remAssign(other: Short) {
        x = (x % other).toShort()
        y = (y % other).toShort()
    }
    operator fun unaryMinus(): Vec2s = Vec2s((-(x.toInt())).toShort(), (-(y.toInt())).toShort())
    operator fun unaryPlus(): Vec2s = Vec2s((+(x.toInt())).toShort(), (+(y.toInt())).toShort())
    operator fun get(index: Int): Short = when (index) {
        0 -> x
        1 -> y
        else -> throw IndexOutOfBoundsException()
    }
    operator fun set(index: Int, value: Short): Unit = when (index) {
        0 -> x = value
        1 -> y = value
        else -> throw IndexOutOfBoundsException()
    }

    fun abs(): Vec2s = Vec2s(kotlin.math.abs(x.toInt()).toShort(), kotlin.math.abs(y.toInt()).toShort())
    fun max(other: Vec2s): Vec2s = Vec2s(kotlin.math.max(x.toInt(), other.x.toInt()).toShort(), kotlin.math.max(y.toInt(), other.y.toInt()).toShort())
    fun min(other: Vec2s): Vec2s = Vec2s(kotlin.math.min(x.toInt(), other.x.toInt()).toShort(), kotlin.math.min(y.toInt(), other.y.toInt()).toShort())
    fun max(other: Short): Vec2s = Vec2s(kotlin.math.max(x.toInt(), other.toInt()).toShort(), kotlin.math.max(y.toInt(), other.toInt()).toShort())
    fun min(other: Short): Vec2s = Vec2s(kotlin.math.min(x.toInt(), other.toInt()).toShort(), kotlin.math.min(y.toInt(), other.toInt()).toShort())
    fun clamp(min: Vec2s, max: Vec2s): Vec2s = Vec2s(kotlin.math.max(min.x.toInt(), kotlin.math.min(max.x.toInt(), x.toInt())).toShort(), kotlin.math.max(min.y.toInt(), kotlin.math.min(max.y.toInt(), y.toInt())).toShort())
    fun clamp(min: Short, max: Short): Vec2s = Vec2s(kotlin.math.max(min.toInt(), kotlin.math.min(max.toInt(), x.toInt())).toShort(), kotlin.math.max(min.toInt(), kotlin.math.min(max.toInt(), y.toInt())).toShort())
    fun clamp(min: Short, max: Vec2s): Vec2s = Vec2s(kotlin.math.max(min.toInt(), kotlin.math.min(max.x.toInt(), x.toInt())).toShort(), kotlin.math.max(min.toInt(), kotlin.math.min(max.y.toInt(), y.toInt())).toShort())
    fun clamp(min: Vec2s, max: Short): Vec2s = Vec2s(kotlin.math.max(min.x.toInt(), kotlin.math.min(max.toInt(), x.toInt())).toShort(), kotlin.math.max(min.y.toInt(), kotlin.math.min(max.toInt(), y.toInt())).toShort())
    fun cross(other: Vec2s): Short = (x * other.y - y * other.x).toShort()
    fun dot(other: Vec2s): Short = (x * other.x + y * other.y).toShort()
    fun length(): Short = kotlin.math.sqrt(x * x + y * y.toDouble()).toInt().toShort()
    fun lengthSquared(): Short = (x * x + y * y).toShort()
    fun normalize(): Vec2s = this / length()
    fun rotate(angle: Double): Vec2s = Vec2s((x * cos(angle) - y * sin(angle)).toInt().toShort(), (x * sin(angle) + y * cos(angle)).toInt().toShort())
    fun rotate(angle: Float): Vec2s = Vec2s((x * cos(angle) - y * sin(angle)).toInt().toShort(), (x * sin(angle) + y * cos(angle)).toInt().toShort())
    fun angle(): Double = kotlin.math.atan2(y.toDouble(), x.toDouble())
    fun angle(other: Vec2s): Double = kotlin.math.atan2(y.toDouble(), x.toDouble()) - kotlin.math.atan2(other.y.toDouble(), other.x.toDouble())



    fun toVec2f(): Vec2f = Vec2f(x.toFloat(), y.toFloat())
    fun toVec2d(): Vec2d = Vec2d(x.toDouble(), y.toDouble())
    fun toVec2b(): Vec2b = Vec2b(x.toByte(), y.toByte())
    fun toVec2s(): Vec2s = Vec2s(x, y)
    fun toVec2i(): Vec2i = Vec2i(x.toInt(), y.toInt())
    fun toVec2l(): Vec2l = Vec2l(x.toLong(), y.toLong())
    fun toVec2ub(): Vec2ub = Vec2ub(x.toUByte(), y.toUByte())
    fun toVec2us(): Vec2us = Vec2us(x.toUShort(), y.toUShort())
    fun toVec2u(): Vec2u = Vec2u(x.toUInt(), y.toUInt())
    fun toVec2ul(): Vec2ul = Vec2ul(x.toULong(), y.toULong())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vec2s) return false

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    operator fun component1(): Short = x
    operator fun component2(): Short = y
}
