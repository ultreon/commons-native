package tk.ultreonteam.commons.vector

import kotlin.math.cos
import kotlin.math.sin

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Vec2i(var x: Int, var y: Int) {
    companion object {
        val ZERO = Vec2i(0, 0)
        val ONE = Vec2i(1, 1)
        val UP = Vec2i(0, -1)
        val DOWN = Vec2i(0, 1)
        val LEFT = Vec2i(-1, 0)
        val RIGHT = Vec2i(1, 0)
        val UP_LEFT = Vec2i(-1, -1)
        val UP_RIGHT = Vec2i(1, -1)
        val DOWN_LEFT = Vec2i(-1, 1)
        val DOWN_RIGHT = Vec2i(1, 1)
    }

    operator fun plus(other: Vec2i): Vec2i = Vec2i(x + other.x, y + other.y)
    operator fun minus(other: Vec2i): Vec2i = Vec2i(x - other.x, y - other.y)
    operator fun times(other: Vec2i): Vec2i = Vec2i(x * other.x, y * other.y)
    operator fun rem(other: Vec2i): Vec2i = Vec2i(x % other.x, y % other.y)
    operator fun div(other: Vec2i): Vec2i = Vec2i(x / other.x, y / other.y)
    operator fun times(other: Int): Vec2i = Vec2i(x * other, y * other)
    operator fun div(other: Int): Vec2i = Vec2i(x / other, y / other)
    operator fun rem(other: Int): Vec2i = Vec2i(x % other, y % other)
    operator fun plusAssign(other: Vec2i) {
        x += other.x
        y += other.y
    }
    operator fun minusAssign(other: Vec2i) {
        x -= other.x
        y -= other.y
    }
    operator fun timesAssign(other: Vec2i) {
        x *= other.x
        y *= other.y
    }
    operator fun divAssign(other: Vec2i) {
        x /= other.x
        y /= other.y
    }
    operator fun remAssign(other: Vec2i) {
        x %= other.x
        y %= other.y
    }
    operator fun timesAssign(other: Int) {
        x *= other
        y *= other
    }
    operator fun divAssign(other: Int) {
        x /= other
        y /= other
    }
    operator fun remAssign(other: Int) {
        x %= other
        y %= other
    }
    operator fun unaryMinus(): Vec2i = Vec2i(-x, -y)
    operator fun unaryPlus(): Vec2i = Vec2i(+x, +y)
    operator fun get(index: Int): Int = when (index) {
        0 -> x
        1 -> y
        else -> throw IndexOutOfBoundsException()
    }
    operator fun set(index: Int, value: Int): Unit = when (index) {
        0 -> x = value
        1 -> y = value
        else -> throw IndexOutOfBoundsException()
    }

    fun abs(): Vec2i = Vec2i(kotlin.math.abs(x), kotlin.math.abs(y))
    fun max(other: Vec2i): Vec2i = Vec2i(kotlin.math.max(x, other.x), kotlin.math.max(y, other.y))
    fun min(other: Vec2i): Vec2i = Vec2i(kotlin.math.min(x, other.x), kotlin.math.min(y, other.y))
    fun max(other: Int): Vec2i = Vec2i(kotlin.math.max(x, other), kotlin.math.max(y, other))
    fun min(other: Int): Vec2i = Vec2i(kotlin.math.min(x, other), kotlin.math.min(y, other))
    fun clamp(min: Vec2i, max: Vec2i): Vec2i = Vec2i(kotlin.math.max(min.x, kotlin.math.min(max.x, x)), kotlin.math.max(min.y, kotlin.math.min(max.y, y)))
    fun clamp(min: Int, max: Int): Vec2i = Vec2i(kotlin.math.max(min, kotlin.math.min(max, x)), kotlin.math.max(min, kotlin.math.min(max, y)))
    fun clamp(min: Int, max: Vec2i): Vec2i = Vec2i(kotlin.math.max(min, kotlin.math.min(max.x, x)), kotlin.math.max(min, kotlin.math.min(max.y, y)))
    fun clamp(min: Vec2i, max: Int): Vec2i = Vec2i(kotlin.math.max(min.x, kotlin.math.min(max, x)), kotlin.math.max(min.y, kotlin.math.min(max, y)))
    fun cross(other: Vec2i): Int = x * other.y - y * other.x
    fun dot(other: Vec2i): Int = x * other.x + y * other.y
    fun length(): Int = kotlin.math.sqrt(x * x + y * y.toDouble()).toInt()
    fun lengthSquared(): Int = x * x + y * y
    fun normalize(): Vec2i = this / length()
    fun rotate(angle: Double): Vec2i = Vec2i((x * cos(angle) - y * sin(angle)).toInt(), (x * sin(angle) + y * cos(angle)).toInt())
    fun rotate(angle: Float): Vec2i = Vec2i((x * cos(angle) - y * sin(angle)).toInt(), (x * sin(angle) + y * cos(angle)).toInt())
    fun angle(): Double = kotlin.math.atan2(y.toDouble(), x.toDouble())
    fun angle(other: Vec2i): Double = kotlin.math.atan2(y.toDouble(), x.toDouble()) - kotlin.math.atan2(other.y.toDouble(), other.x.toDouble())



    fun toVec2f(): Vec2f = Vec2f(x.toFloat(), y.toFloat())
    fun toVec2d(): Vec2d = Vec2d(x.toDouble(), y.toDouble())
    fun toVec2b(): Vec2b = Vec2b(x.toByte(), y.toByte())
    fun toVec2s(): Vec2s = Vec2s(x.toShort(), y.toShort())
    fun toVec2i(): Vec2i = Vec2i(x, y)
    fun toVec2l(): Vec2l = Vec2l(x.toLong(), y.toLong())
    fun toVec2ub(): Vec2ub = Vec2ub(x.toUByte(), y.toUByte())
    fun toVec2us(): Vec2us = Vec2us(x.toUShort(), y.toUShort())
    fun toVec2u(): Vec2u = Vec2u(x.toUInt(), y.toUInt())
    fun toVec2ul(): Vec2ul = Vec2ul(x.toULong(), y.toULong())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vec2i) return false

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    operator fun component1(): Int = x
    operator fun component2(): Int = y
}
