package tk.ultreonteam.commons.vector

import kotlin.math.cos
import kotlin.math.sin

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Vec2d(var x: Double, var y: Double) {
    companion object {
        val ZERO = Vec2d(0.0, 0.0)
        val HALF = Vec2d(0.5, 0.5)
        val ONE = Vec2d(1.0, 1.0)
        val UP = Vec2d(0.0, -1.0)
        val DOWN = Vec2d(0.0, 1.0)
        val LEFT = Vec2d(-1.0, 0.0)
        val RIGHT = Vec2d(1.0, 0.0)
        val UP_LEFT = Vec2d(-1.0, -1.0)
        val UP_RIGHT = Vec2d(1.0, -1.0)
        val DOWN_LEFT = Vec2d(-1.0, 1.0)
        val DOWN_RIGHT = Vec2d(1.0, 1.0)
    }

    operator fun plus(other: Vec2d): Vec2d = Vec2d(x + other.x, y + other.y)
    operator fun minus(other: Vec2d): Vec2d = Vec2d(x - other.x, y - other.y)
    operator fun times(other: Vec2d): Vec2d = Vec2d(x * other.x, y * other.y)
    operator fun rem(other: Vec2d): Vec2d = Vec2d(x % other.x, y % other.y)
    operator fun div(other: Vec2d): Vec2d = Vec2d(x / other.x, y / other.y)
    operator fun times(other: Double): Vec2d = Vec2d(x * other, y * other)
    operator fun div(other: Double): Vec2d = Vec2d(x / other, y / other)
    operator fun rem(other: Double): Vec2d = Vec2d(x % other, y % other)
    operator fun plusAssign(other: Vec2d) {
        x += other.x
        y += other.y
    }
    operator fun minusAssign(other: Vec2d) {
        x -= other.x
        y -= other.y
    }
    operator fun timesAssign(other: Vec2d) {
        x *= other.x
        y *= other.y
    }
    operator fun divAssign(other: Vec2d) {
        x /= other.x
        y /= other.y
    }
    operator fun remAssign(other: Vec2d) {
        x %= other.x
        y %= other.y
    }
    operator fun timesAssign(other: Double) {
        x *= other
        y *= other
    }
    operator fun divAssign(other: Double) {
        x /= other
        y /= other
    }
    operator fun remAssign(other: Double) {
        x %= other
        y %= other
    }
    operator fun unaryMinus(): Vec2d = Vec2d(-x, -y)
    operator fun unaryPlus(): Vec2d = Vec2d(+x, +y)
    operator fun get(index: Int): Double = when (index) {
        0 -> x
        1 -> y
        else -> throw IndexOutOfBoundsException()
    }
    operator fun set(index: Int, value: Double): Unit = when (index) {
        0 -> x = value
        1 -> y = value
        else -> throw IndexOutOfBoundsException()
    }

    fun abs(): Vec2d = Vec2d(kotlin.math.abs(x), kotlin.math.abs(y))
    fun max(other: Vec2d): Vec2d = Vec2d(kotlin.math.max(x, other.x), kotlin.math.max(y, other.y))
    fun min(other: Vec2d): Vec2d = Vec2d(kotlin.math.min(x, other.x), kotlin.math.min(y, other.y))
    fun max(other: Double): Vec2d = Vec2d(kotlin.math.max(x, other), kotlin.math.max(y, other))
    fun min(other: Double): Vec2d = Vec2d(kotlin.math.min(x, other), kotlin.math.min(y, other))
    fun clamp(min: Vec2d, max: Vec2d): Vec2d = Vec2d(kotlin.math.max(min.x, kotlin.math.min(max.x, x)), kotlin.math.max(min.y, kotlin.math.min(max.y, y)))
    fun clamp(min: Double, max: Double): Vec2d = Vec2d(kotlin.math.max(min, kotlin.math.min(max, x)), kotlin.math.max(min, kotlin.math.min(max, y)))
    fun clamp(min: Double, max: Vec2d): Vec2d = Vec2d(kotlin.math.max(min, kotlin.math.min(max.x, x)), kotlin.math.max(min, kotlin.math.min(max.y, y)))
    fun clamp(min: Vec2d, max: Double): Vec2d = Vec2d(kotlin.math.max(min.x, kotlin.math.min(max, x)), kotlin.math.max(min.y, kotlin.math.min(max, y)))
    fun cross(other: Vec2d): Double = x * other.y - y * other.x
    fun dot(other: Vec2d): Double = x * other.x + y * other.y
    fun length(): Double = kotlin.math.sqrt(x * x + y * y)
    fun lengthSquared(): Double = x * x + y * y
    fun normalize(): Vec2d = this / length()
    fun rotate(angle: Double): Vec2d = Vec2d((x * cos(angle) - y * sin(angle)), (x * sin(angle) + y * cos(angle)))
    fun rotate(angle: Float): Vec2d = Vec2d((x * cos(angle) - y * sin(angle)), (x * sin(angle) + y * cos(angle)))
    fun angle(): Double = kotlin.math.atan2(y, x)
    fun angle(other: Vec2d): Double = kotlin.math.atan2(y, x) - kotlin.math.atan2(other.y, other.x)



    fun toVec2f(): Vec2f = Vec2f(x.toFloat(), y.toFloat())
    fun toVec2d(): Vec2d = Vec2d(x, y)
    fun toVec2b(): Vec2b = Vec2b(x.toInt().toByte(), y.toInt().toByte())
    fun toVec2s(): Vec2s = Vec2s(x.toInt().toShort(), y.toInt().toShort())
    fun toVec2i(): Vec2i = Vec2i(x.toInt(), y.toInt())
    fun toVec2l(): Vec2l = Vec2l(x.toLong(), y.toLong())
    fun toVec2ub(): Vec2ub = Vec2ub(x.toInt().toUByte(), y.toInt().toUByte())
    fun toVec2us(): Vec2us = Vec2us(x.toInt().toUShort(), y.toInt().toUShort())
    fun toVec2u(): Vec2u = Vec2u(x.toUInt(), y.toUInt())
    fun toVec2ul(): Vec2ul = Vec2ul(x.toULong(), y.toULong())

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Vec2d) return false

        if (x != other.x) return false
        if (y != other.y) return false

        return true
    }

    override fun hashCode(): Int {
        var result = x.hashCode()
        result = 31 * result + y.hashCode()
        return result
    }

    operator fun component1(): Double = x
    operator fun component2(): Double = y
}
