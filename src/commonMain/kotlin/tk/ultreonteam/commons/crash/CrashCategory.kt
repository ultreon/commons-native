package tk.ultreonteam.commons.crash

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
open class CrashCategory {
    val entries: ArrayList<CrashEntry> = ArrayList()
    protected val details: String?
    var throwable: Throwable?

    constructor(details: String?) : this(details, null)

    constructor(details: String?, throwable: Throwable?) {
        this.details = details
        this.throwable = throwable
    }

    open fun add(key: String, value: Any?) {
        require(!key.contains(":")) { "Key cannot contain a colon" }
        require(key.length <= 32) { "Key cannot be longer than 32 characters." }
        entries.add(CrashEntry(key, value?.toString() ?: "null@0"))
    }

    open fun getDetails(): String? {
        return details
    }

    open fun getThrowable(): Throwable? {
        return throwable
    }

    override fun toString(): String {
        val sb = StringBuilder()
        sb.append(details).append(": \r\n")
        if (entries.size > 0) {
            val simpleEntries: ArrayList<CrashEntry> =
                ArrayList(entries)
            for (i in 0 until simpleEntries.size - 1) {
                val entry: CrashEntry = simpleEntries[i]
                sb.append("   ")
                sb.append(entry.key)
                sb.append(": ")
                sb.append(entry.value)
                sb.append("\r\n")
            }
            val entry: CrashEntry = simpleEntries[simpleEntries.size - 1]
            sb.append("   ")
            sb.append(entry.key)
            sb.append(": ")
            sb.append(entry.value)
            sb.append("\r\n")
        }
        throwable?.getStackTrace()?.forEach {
            sb.append("   ")
            sb.append(it)
            sb.append("\r\n")
        }
        sb.append("\r\n")
        return sb.toString()
    }

    inner class CrashEntry(val key: String, val value: String) {

        override fun toString(): String {
            return "$key: $value"
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (other == null) return false
            if (CrashEntry::class != other::class) return false

            other as CrashEntry

            if (key != other.key) return false
            if (value != other.value) return false

            return true
        }

        override fun hashCode(): Int {
            var result = key.hashCode()
            result = 31 * result + value.hashCode()
            return result
        }


    }
}
