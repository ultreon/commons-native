package tk.ultreonteam.commons.crash

import com.soywiz.klock.DateTime
import com.soywiz.klock.DateTimeTz
import com.soywiz.korio.file.VfsFile
import com.soywiz.korio.file.VfsOpenMode
import com.soywiz.korio.file.std.cwdVfs
import com.soywiz.korio.lang.IOException
import com.soywiz.korio.stream.writeString

@Suppress("MemberVisibilityCanBePrivate")
class CrashLog : CrashCategory {
    private val categories: MutableList<CrashCategory> = ArrayList()

    constructor(details: String, report: CrashLog) : super(details) {
        throwable = addCrashLog(report).throwable
    }

    constructor(details: String, report: CrashLog?, t: Throwable) : this(details, t) {
        report?.let { addCrashLog(it) }
    }

    constructor(details: String, t: Throwable) : super(details, t) {

    }

    private fun addCrashLog(log: CrashLog): CrashLog {
        val cat = CrashCategory(log.getDetails()!!, log.getThrowable())
        cat.entries.clear()
        cat.entries.addAll(log.entries)
        addCategory(cat)
        for (category in log.getCategories()) {
            addCategory(category)
        }
        return log
    }

    private fun addCrash(exception: ApplicationCrash): CrashLog? {
        val crashLog: CrashLog = exception.crashLog
        val crashLog1 = CrashLog(crashLog.details!!, crashLog.throwable!!)
        crashLog1.categories.addAll(crashLog.categories.subList(0, crashLog.categories.size - 1))
        crashLog1.entries.addAll(crashLog.entries)
        return addCrashLog(crashLog1)
    }

    override fun getThrowable(): Throwable {
        return throwable!!
    }

    fun addCategory(crashCategory: CrashCategory) {
        categories.add(crashCategory)
    }

    fun getCategories(): List<CrashCategory> {
        return categories.toList()
    }

    private fun getFinalForm(): CrashLog {
        val crashLog = CrashLog(details!!, throwable!!)
        crashLog.categories.addAll(categories)
        crashLog.entries.addAll(entries)
        val category = CrashCategory("System Details")
        category.add("OS", Platform.osFamily.name.lowercase())
        category.add("CPU", "${Platform.cpuArchitecture.name.lowercase()} ${Platform.cpuArchitecture.bitness}-bit")
        crashLog.addCategory(category)

        return crashLog
    }

    fun createCrash(): ApplicationCrash {
        return ApplicationCrash(getFinalForm())
    }

    override fun toString(): String {
        val s1 = """
             ${"// $details"}
             
             """.trimIndent()
        val cs = StringBuilder()
        val sb = StringBuilder()
        if (entries.size > 0) {
            sb.append("Details:").append("\r\n")
            for (entry in entries) {
                sb.append("  ").append(entry.key)
                sb.append(": ")
                sb.append(entry.value)
                sb.append("\r\n")
            }
        }
        for (category in categories) {
            cs.append("\r\n")
                .append("=------------------------------------------------------------------=")
            cs.append("\r\n").append(category.toString())
        }
        cs.append("=------------------------------------------------------------------=")
        return ">>> C R A S H   R E P O R T <<<\r\n$s1\r\n${throwable!!.stackTraceToString()}$cs\r\n$sb"
    }

    fun getDefaultFileName(): String {
        val now: DateTimeTz = DateTime.nowLocal()
        return "Crash [${now.format("yyyy-MM-dd HH.mm.ss")}].txt"
    }

    fun defaultSave() {
        val file = cwdVfs["Crashes"]
        suspend {
            if (!file.exists()) {
                try {
                    file.mkdirs()
                } catch (e: IOException) {
                    throw RuntimeException(e)
                }
            }
            writeToFile(file[getDefaultFileName()])
        }
    }

    suspend fun writeToFile(file: VfsFile) {
        try {
            file.openUse(VfsOpenMode.CREATE_OR_TRUNCATE) { }
            file.openUse(VfsOpenMode.WRITE) {
                writeString(toString())
                close()
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }
}
