package tk.ultreonteam.commons.crash

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class ApplicationCrash(val crashLog: CrashLog) {
    companion object {
        private val crashHandlers: MutableList<CrashHandler> = mutableListOf()
    }

    fun printCrash() {
        val crashString = crashLog.toString()
        val strings: List<String> = crashString.lines()
        for (string in strings) {
            println(string)
        }
    }

    private fun crash() {
        for (handler in crashHandlers) {
            handler.run()
        }
    }

    fun onCrash(handler: CrashHandler) {
        crashHandlers.add(handler)
    }

    fun getCrashLog(): CrashLog {
        return crashLog
    }

    fun interface CrashHandler {
        fun run()
    }
}