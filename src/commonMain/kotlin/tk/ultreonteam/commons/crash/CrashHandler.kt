package tk.ultreonteam.commons.crash

fun interface CrashHandler {
    fun handle()
}
