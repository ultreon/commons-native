package tk.ultreonteam.commons.exceptions

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class OneTimeUseException : RuntimeException {
    constructor() : super()
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)
}