package tk.ultreonteam.commons.util

/**
 * Returns the count of a given character in a string.
 *
 * @param c The character to count.
 * @return The count of the character in the string.
 * @author Qboi123
 * @since 1.0
 */
fun String.count(c: Char): Int {
    var count = 0
    for (element in this) {
        if (element == c) {
            count++
        }
    }
    return count
}

/**
 * Returns the index of the first whitespace character or '-' in `line`
 * that is at or before `start`. Returns -1 if no such character is
 * found.
 *
 * @param line  a string
 * @param start where to star looking
 * @return the index of the first whitespace character or '-' in `line` that is at or before `start` or -1 if no such character is found
 * @author Qboi123
 * @since 1.0
 */
fun findBreakBefore(line: String, start: Int): Int {
    for (i in start downTo 0) {
        val c = line[i]
        if (c.isWhitespace() || c == '-') return i
    }
    return -1
}

/**
 * Returns the index of the first whitespace character or '-' in `line`
 * that is at or after `start`. Returns -1 if no such character is
 * found.
 *
 * @param line  a string
 * @param start where to star looking
 * @return the index of the first whitespace character or '-' in `line` that is at or after `start` or -1 if no such character is found
 * @author Qboi123
 * @since 1.0
 */
fun findBreakAfter(line: String, start: Int): Int {
    val len = line.length
    for (i in start until len) {
        val c = line[i]
        if (c.isWhitespace() || c == '-') return i
    }
    return -1
}

/**
 * Returns an array of strings, one for each line in the string. Lines end
 * with any of cr, lf, or cr lf. A line ending at the end of the string will
 * not output a further, empty string.
 *
 * @param str the string to split
 * @return a non-empty list of strings
 * @author Qboi123
 * @since 1.0
 */
fun splitIntoLines(str: String): List<String> {
    val strings: ArrayList<String> = ArrayList<String>()
    val len = str.length
    if (len == 0) {
        strings.add("")
        return strings
    }
    var lineStart = 0
    var i = 0
    while (i < len) {
        val c = str[i]
        if (c == '\r') {
            var newlineLength = 1
            if (i + 1 < len && str[i + 1] == '\n') newlineLength = 2
            strings.add(str.substring(lineStart, i))
            lineStart = i + newlineLength
            if (newlineLength == 2) // skip \n next time through loop
                ++i
        } else if (c == '\n') {
            strings.add(str.substring(lineStart, i))
            lineStart = i + 1
        }
        ++i
    }
    if (lineStart < len) strings.add(str.substring(lineStart))
    return strings
}
