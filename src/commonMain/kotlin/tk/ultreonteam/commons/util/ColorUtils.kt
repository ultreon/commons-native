@file:Suppress("unused")

package tk.ultreonteam.commons.util

import tk.ultreonteam.commons.exceptions.InvalidValueException
import tk.ultreonteam.commons.lang.Color


/**************************************************************************
 * Converts a color hex code (e.g. "#FFFFFF ) to a color instance.
 *
 * @param hex e.g. "#FFFFFF" or with alpha "#FFFFFF00"
 * @return a new Color instance based on the color hex code
 * @see Color
 *
 * @see .unpackHex
 */
@Deprecated("use unpackHex(...) instead.", ReplaceWith("unpackHex(hex)"))
fun hex2Rgb(hex: String): Color {
    return unpackHex(hex)
}

/**************************************************************************
 * Converts a color hex code (e.g. "#FFFFFF ) to a color instance.
 *
 * @param hex e.g. "#FFFFFF" or with alpha "#FFFFFF00"
 * @return a new Color instance based on the color hex code
 * @see Color
 */
fun unpackHex(hex: String): Color {
    return if (Regex("#[\\da-fA-F]{6}").matches(hex)) {
        val rgb: Int = hex.substring(1).toInt(16)
        Color(rgb, false)
    } else if (Regex("#[\\da-fA-F]{8}").matches(hex)) {
        val rgb: Int = hex.substring(1).toInt(16)
        Color(rgb, true)
    } else if (Regex("#[\\da-fA-F]{3}").matches(hex)) {
        val rgb: Int = charArrayOf(hex[1], hex[1], hex[2], hex[2], hex[3], hex[3])
            .concatToString()
            .toInt(16)
        Color(rgb, false)
    } else if (Regex("#[\\da-fA-F]{4}").matches(hex)) {
        val rgb: Int = charArrayOf(hex[1], hex[1], hex[2], hex[2], hex[3], hex[3], hex[4], hex[4])
            .concatToString()
            .toInt(16)
        Color(rgb, true)
    } else {
        if (hex.isNotEmpty()) {
            if (hex[0] != '#') {
                throw InvalidValueException("First character of color code isn't '#'.")
            } else if (hex.length != 3 && hex.length != 4 && hex.length != 6 && hex.length != 8) {
                throw InvalidValueException("Invalid hex length, should be 3, 4, 6 or 8 in length.")
            } else {
                throw InvalidValueException("Invalid hex value. Hex values may only contain numbers and letters a to f.")
            }
        } else {
            throw InvalidValueException("The color hex is empty, it should start with a hex, and then 3, 4, 6 or 8 hexadecimal digits.")
        }
    }
}

fun multiConvertHexToRgb(vararg colorStrings: String): Array<Color> {
    val colors: ArrayList<Color> = ArrayList()
    for (colorStr in colorStrings) {
        colors.add(unpackHex(colorStr))
    }
    return colors.toTypedArray()
}

/**
 * Parse a color string into a color array.  
 * **Note: *this doesn't add the ‘#’ prefixes, so use only hex digits for the colors.***  
 *   
 * Same as `parseColorString(colorString, false)`.  
 *
 * @param colorString the color string, hex colors separated by a comma.
 * @return an array of colors parsed from the color string.
 */
fun parseColorString(colorString: String): Array<Color> {
    return parseColorString(colorString, false)
}

/**
 * Parse a color string into a color array.  
 *
 * @param colorString the color string, hex colors separated by a comma.
 * @param addPrefix   add the ‘#’ for every item in the color string.
 * @return an array of colors parsed from the color string.
 */
fun parseColorString(colorString: String, addPrefix: Boolean): Array<Color> {
    val strings = colorString.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
    if (addPrefix) {
        for (i in strings.indices) {
            strings[i] = "#" + strings[i]
        }
    }
    return multiConvertHexToRgb(*strings)
}
