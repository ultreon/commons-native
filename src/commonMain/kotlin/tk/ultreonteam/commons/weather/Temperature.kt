package tk.ultreonteam.commons.weather

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class TempKelvin(val value: Double) {
    val celcius: Double
        get() = value - 273.15
    val fahrenheit: Double
        get() = (celcius * 9 / 5) - 495.67
}


@CStruct.CPlusPlusClass
class TempCelsius(val value: Double) {
    val kelvin: Double
        get() = value + 273.15
    val fahrenheit: Double
        get() = (value * 9 / 5) + 32
}


@CStruct.CPlusPlusClass
class TempFahrenheit(val value: Double) {
    val kelvin: Double
        get() = (value + 459.67) * 5 / 9
    val celsius: Double
        get() = (value - 32) * 5 / 9
}
