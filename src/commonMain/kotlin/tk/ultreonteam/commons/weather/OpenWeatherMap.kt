package tk.ultreonteam.commons.weather

import com.soywiz.korio.file.std.UrlVfsJailed
import kotlinx.cinterop.internal.CStruct
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonObject
import tk.ultreonteam.commons.weather.openweathermap.Weather


@CStruct.CPlusPlusClass
class OpenWeatherMap {
    private var apiKey: String

    constructor(apiKey: String) {
        this.apiKey = apiKey
    }

    private val json: Json = Json { ignoreUnknownKeys = true }

    suspend fun getWeather(city: String): Weather {
        val url = "http://api.openweathermap.org/data/2.5/weather?q=$city&APPID=$apiKey"
        val data = getJSON(url)
        println("data = $data")
        json.parseToJsonElement(data).jsonObject.let {
            return Weather(it)
        }
    }

    fun getWeatherSync(city: String): Weather {
        return runBlocking {
            getWeather(city)
        }
    }

    private suspend fun getJSON(url: String): String {
        return UrlVfsJailed(url).readString()
    }

    companion object {
        const val API_URL = "http://api.openweathermap.org/data/2.5/weather"
    }
}