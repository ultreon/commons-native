package tk.ultreonteam.commons.devices

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
data class DisplayMode(val width: Int, val height: Int, val bitsPerPixel: Int)