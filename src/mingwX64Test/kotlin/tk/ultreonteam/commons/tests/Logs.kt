package tk.ultreonteam.commons.tests

import kotlin.reflect.KProperty

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Logs<T : Any?>(var value: T) {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = value
    }
}

fun <T : Any?> logs(value: T): Logs<T> = Logs(value)
