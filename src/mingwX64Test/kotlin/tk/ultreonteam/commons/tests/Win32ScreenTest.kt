package tk.ultreonteam.commons.tests

import kotlinx.cinterop.alloc
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.ptr
import platform.windows.GetCursorPos
import platform.windows.POINT
import platform.windows.SetCursorPos
import platform.windows.Sleep
import tk.ultreonteam.commons.devices.Win32Screen
import tk.ultreonteam.commons.lang.Color
import tk.ultreonteam.commons.vector.Vec2f
import tk.ultreonteam.commons.vector.Vec2i
import kotlin.random.Random
import kotlin.test.Test

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Win32ScreenTest {
    private val screen = Win32Screen()

    @Test
    fun testPixel() {
        println(screen.getPixel(0, 0))
        for (x in 0 until screen.width * 2) {
            for (y in 0 until screen.height) {
                screen.setPixel(x, y, Color(0xff0000))
            }
        }
    }

    @Test
    fun testSize() {
        println(screen.screenSize)
        println(screen.width)
        println(screen.height)
    }

    @Test
    fun glitch2() {
        val (w, h) = screen.screenSize
        for (y in 0 until h) {
            for (x in 0 until w) {
                screen.setPixel(x, y, Color(Random.nextInt(0xffffff)))
            }
        }
    }

    @Test
    fun glitch3() {
        // Not implemented yet
    }

    @Test
    fun testRandomMouse() {
        while (true) {
            getCursorPos().toVec2f().plus(Vec2f(Random.nextFloat() * 6 - 3, Random.nextFloat() * 6 - 3)).let {
                SetCursorPos(it.x.toInt(), it.y.toInt())
            }

            Sleep(100)
        }
    }

    fun getCursorPos(): Vec2i {
        memScoped {
            val point = alloc<POINT>()
            GetCursorPos(point.ptr)
            return Vec2i(point.x, point.y)
        }
    }

    class CursorPosition {
        var x: Int
        var y: Int

        constructor(x: Int = 0, y: Int = 0) {
            this.x = x
            this.y = y
        }

        constructor(point: POINT) {
            this.x = point.x
            this.y = point.y
        }

    }
}
