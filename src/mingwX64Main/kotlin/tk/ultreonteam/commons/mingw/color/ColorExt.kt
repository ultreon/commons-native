package tk.ultreonteam.commons.mingw.color

import tk.ultreonteam.commons.lang.Color
import platform.windows.*

fun Color.toColorRef(): COLORREF {
    return rgbReversed.toUInt()
}
