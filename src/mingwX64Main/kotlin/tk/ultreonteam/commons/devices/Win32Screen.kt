package tk.ultreonteam.commons.devices

import platform.windows.*
import tk.ultreonteam.commons.lang.Color
import tk.ultreonteam.commons.lang.Pixel
import tk.ultreonteam.commons.mingw.color.toColorRef
import tk.ultreonteam.commons.vector.Vec2i

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Win32Screen {
    val screenSize: Vec2i
        get() = Vec2i(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN))
    private val dc: HDC = GetDC(GetDesktopWindow())!!

    val width
        get() = GetSystemMetrics(SM_CXSCREEN)
    val height
        get() = GetSystemMetrics(SM_CYSCREEN)
    fun getDpi() = GetSystemMetrics(4)
    fun getPixel(x: Int, y: Int): Pixel {
        return Pixel(x, y, Color(GetPixel(dc, x, y), reverse = true))
    }
    fun setPixel(x: Int, y: Int, color: Color) {
        SetPixel(dc, x, y, color.toColorRef())
    }
//    fun getDisplayModes(): List<DisplayMode> {
//        val modes = ArrayList<DisplayMode>()
//        var i: UInt = 0u
//
//        try {
//            while (true) {
//                val mode = DisplayMode()
////                val mode1 = _devicemodeW()
//                if (!EnumDisplaySettingsW(null, i, mode1.pointed)) break
//                modes.add(mode)
//                i++
//            }
//        } catch (e: Exception) {
//            // pass
//        }
//
//        return modes.toList()
//    }
}