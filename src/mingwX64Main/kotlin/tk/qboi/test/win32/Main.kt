package tk.qboi.test.win32

import platform.windows.FindWindowA

import kotlinx.cinterop.internal.CStruct


@CStruct.CPlusPlusClass
class Main {
    fun doStuff() {
        println("Hello, world!")
    }
}

fun main() {
    Main().doStuff()
}
